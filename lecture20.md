## Quiz        

1. The continuous consistency
    - Conit
    - Operation
        - The metadata of these operations are going to be keeping track of what we have done to the values X&Y that are inside of this comment
        - The metadata is a way to quantify the inconsistency. It is a way to observe the state of the replicas and to make a quantitative objective observation about what has happened at these replicas, so that we can compare the quantities
    - Result

2. You cannot see the replica data. So you can only work off of that data that you have received. It is how we set the **vector Clock**

3. The **order deviation** is the number of operations that have not been committed at the replica
    - The committed is that it has been written to non volatile storage. We cannot change it

4. The numerical deviation
    - The numerical deviation based on operation
        - It is the count of operations that have occurred remotely. So the replica has not observed
    - The numerical deviation based on value
        - Compare the x and y that is committed locally with the x and y that are presently in memory remotely. You take the largest difference

5. The vector clock, order deviation, and numerical deviation  
    - The first number at the **vector clock** is the number of operations that have originated at the replica A
    - Two operations cannot happen simultaneously, so we have to add 1 to it
    - <img src="img/vector clock position 1.png" alt="vector clock position 1" width="500" height="600">
    - That second number at the **vector clock** is the last operation that replica A has observed from replica B
    - <img src="img/vector clock position 2.png" alt="vector clock position 2" width="500" height="600">
    - The replica B has not seen anything from the replica A and it assume that the replica A has not been done anything. So the first number in **vector clock** is 0
    - <img src="img/vector clock B position 1.png" alt="vector clock B position 1" width="500" height="600">
    - The replica B has an operation at time 10 and it cannot have two operations happen simultaneously. so we have to add 1 to it. So second number in **vector clock** is 11
    - <img src="img/vector clock B position 2.png" alt="vector clock B position 2" width="500" height="600">
    - There are two operations that have not been grayed out. So there are two operations we can rearrange if we needed to. So the **order deviation** in replica A is 2 and in replica B is 2
    - <img src="img/order deviation.png" alt="order deviation" width="500" height="600">
    - The first number in the **numerical deviation** is the numerical deviation based on operation
    - The replica A has seen 5, B, it has not seen 10, B. So it is 1
    - The second number in the **numerical deviation** is the numerical deviation based on value
    - Compare the committed x, y in replica A with the conit x, y in replica B. The largest difference is 3
    - <img src="img/numerical deviation.png" alt="numerical deviation" width="500" height="600">          
    - There is 3 operation in replica A and replica B has not seen it and the numerical deviation based on operation is 3
    - Compare the committed x, y in replica B with the conit x, y in replica A. The largest difference is 6
    - <img src="img/numerical deviation B.png" alt="numerical deviation B" width="500" height="600">
    - The vector clock
        - In A, the first number is the last A plus 1. The second number is the last B
        - In B, the first number is the last A. If there is no A, it is 0. The second number is the last B plus 1
    - The order deviation is that not grayed out 
    - The numerical deviation 
        - The first number. In A, it is the difference between B in replica A and B in replica B. In B, it is the difference between A in replica B and A in replica A
        - The second number. The largest difference between the locally committed and the remotely conit
    - <img src="img/vector clock order deviation numerical deviation.png" alt="vector clock order deviation numerical deviation" width="500" height="600">

6. We want to quantify the state of these replicas.
    - The quantify the state is that we can put definitive objective upper bounds on the amount of divergance we're going to tolerate

7. If the granularity is too large and you hit one of those upper bounds with regards to consistency, when you trigger that update, it has to be applied to everything

8. The sequential consistency
    - W(x)a
        - It is that write the value a into the variable X
    - R(x)a
        - It is that read that X and observe the a
    - R(x)NIL
        - It is that reads something from X and initially doesn't see anything. It sees that null value
        - If it see null, it does not receive anything
    - <img src="img/sequential consistency.png" alt="sequential consistency" width="500" height="600">

9. The networks never propagate things instantly and they propagate things out of order

10. The sequential ordering
    - It is that it doesn't necessarily matter the order that you observe those operations in as long as all of the processes observe that same ordering
    - <img src="img/sequential ordering.png" alt="sequential ordering" width="500" height="600">
    - It is **in sequency**
    - It is order **specified by the program**
    - The operations occurred in the middle. Sequential consistency says that's fine. Any orderings is OK. As long as they are happening in the order **specified by their program** and as long as all of the processes see the same order
    - print(x, z) cannot happen before y = 1
    - <img src="img/sequential ordering process.png" alt="sequential ordering process" width="500" height="600">





































