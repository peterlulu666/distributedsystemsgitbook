## Quiz        

1. The network layer
    - It is in every host, router.
    - A network-layer packet is a datagram.
    - A router forwards a packet based on the packet’s IP address.
    - A link-layer switch forwards a packet based on the packet’s MAC address.

2. The transport lay and network layer just run on in system.

3. The routing 
    - Determine the best path through the network.
    - The best path is the least cost path. 

4. The forwarding 
    - physically taking data from one port on the router to another port on the same router.      

5. What is the difference between routing and forwarding?
    - Forwarding is about moving a packet from a router’s input port to the appropriate output port. Routing is about determining the end-to-routes between sources and destinations.        

5. The routing algorithm 
    - The least cost path.
    - The end to end path.

6. The network-layer functions in a datagram network
    - The forwarding and routing.

6. The network-layer functions in a virtual- circuit network
    - The call setup.

6. The local forwarding table
    - The local forwarding at this router.      

7. The network service model
    - Guarantee delivery.
    - Internet guarantee nothing.
    - The best effort, try once.

8. The virtual circuit
    - Guarantee traffic model. 
    - It is reliable, but it does not scale. If we want to communicate one another. There are a lot connection.  

9. Why could virtual circuit network offer quality of service minimum?
    - They can guarantees resources at every hop along the way. 

10. How many IP does the router have?
    - One for each port
    - It has one address for every interface.      
    - It is equal to the number of the router interface.

11. The datagram network is the packet switch network
    - There is no call setup.
    - There is no end to end path.
    - Some of the data will take different path. Receive package out of order.        

12. The longest prefix matching.

13. The datagram and ATM VC virtual circuit.

14. The subnet
    - The miniature network to use.
    - Every isolated network.

15. How does the host get IP?
    - hand code by system admin.
    - DHCP.

16. The DHCP will return more than IP.



















