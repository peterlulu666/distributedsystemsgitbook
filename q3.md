## Quiz

1. The transaction 
    - The sequence of operation.      

2. The ACID ensure that the operation succeed
    - The atomic operation
        - All operation succeed or fail. If the transaction fails, we will treat it as if nothing happens. We have to reset the system that it had before we tried to perform the transaction.   
        - We will treat the multiple operation as if there is one indivisible operation.
        - We either have to do every operation correctly or we have to reset all the operation.
        - The operation fail is called abort, it will reset the operation.            
    - The consistency operation
        - We have to make sure that all the operation work correctly.      
    - The isolation operation
        - If there is overlap of data we are going to to read and write. We will lock the data. We will make sure that only our transaction will modify it and make sure we do not interfere with other transaction.          
    - The durability operation
        - We write the data on the non volatile storage. If everything works, we will commit it.      

3. The TP Monitor
    - The decentralized system.        

4. The communication middleware
    - The remote procedure call.
    - The message oriented middleware.   

5. The pervasive system
    - They are divied into three category
        - The ubiquitous, everywhere you go, you can interact with the computer. It is waiting there in the background.        
        - The mobile, the system will not always be there, it change address and it change the access point.        
        - The sensor network, weather, traffic, noise, and pollution environment.    
            - The participatory sensing, waze.
    - The transparancy is not in the pervasive system
        - It is not true. 
        - There is the migration transparency. The components are mobile and will need to re-establish connections when moving from one access point to another. Preferably, such handovers should be completely transparent to the user.  
    - There is more pervasive system
        - The home systems, electronic health-care systems, and sensor networks. 
        - Think of large- scale wireless mesh networks in cities or neighborhoods that provide services such as Internet access, but also form the basis for other services like a news system. 
        - There are systems for habitat monitoring, electronic jails by which offenders are continuously monitored, large-scale integrated sports systems, office systems deploying active badges to know about the whereabouts of their employees. 

6. The ubiquitous
    - It is networked, distributed, and accessible in a transparent manner.        
    - The interaction is unobtrusive.      
        - The voice interaction.      
        - The facial recognition.
    - The context awareness, the system will know what you are doing.          
        - The light sensor.
    - The autonomy.
        - There is no human intervention.      
    - The intelligence.
        - The system will cope with the dynamic action and interaction.        

7. The mobile
    - There is wireless communication.      
    - It will change the location.      
    - It is the disruption tolerant.      

8. The sensor network
    - The phone sensor.      

9. The architecture
    - The logically different system.      
        - The layer, modular
        - There is hierarchy for the layering.
        - There is no hierarchy for the object. There is direction for the object.        

10. The exchange data
    - The publish subscribe, decoupled in space.    
    - The shared dataspace, decoupled in time.      

11. The anonymous in term of information security
    - There is mechanism that will prevent you from obtaining the IP address and identifier info.      

12. The anonymous
    - We do not have use the IP address and the identifier.      

13. The decoupled in space is not the anonymous.
    - The anonymous is that we do not know who we are sending the data to.        
    - The decoupled in space is that we do not have explicit identifier when we send the data.      

14. The decoupled in time, asynchronous
    - The subscribed component does not have to be active and connected to the medium to receive the data.      









1. The layering
    - The user interface layer
    - The processing layer
    - The data layer

2. The single tiered, two tiered, and three tiered.

3. The structured P2P network
    - The connect node based on the criteria. 

4. The unstructured P2P network
    - The pick the node randomly.      

5. The superpeers role in the hybrid P2P network
    - It will maintain the index.
    - It will monitor the state of the network.    
    - It will track the system state.
    - It will track which node will have the data for you.      

6. The search unstructured P2P network, the flooding
    - It is the broadcast. It is the brute force solution. You send lookup query to the neighbor node. If all the neighbor node do not have the data you want, you will repeat the requesting until the lookup request is flooded.   
    - The problem of the flooding is that there is a lot of data going back and forth. So there is the redundant recipients. It does not accomplish anything with that amount of overhead.      
    - The limited flooding is that you will repeat the look up query with a fixed number of time.      
    - The probabilistic flooding is that you will repeat the look up query with a certain probability.      

7. The search unstructured P2P network, the random walk
    - It pick a neighbor at random. If the neighbor has the answer, it reply. Otherwise, it pick neighbor.       
    - It will work well with the replicated data.
    - Instead of looking for one data, there is the second data to look up.      
    - The benefit of the random walk is that it reduce the overhead.
    - The problem of the random walk is that it take a long time to find it.       

8. The content distribution network CDN
    - The challenge stream content to people simultaneously
        - The option 1, mega server, it does not scale.      
        - The option 2, multiple copy.
            - The enter deep.
            - The bring home.

9. The CDN cluster selection strategy
    - The challenge CDN DNS select CDN node to stream to the client.
    - The alternative, let the client select.      





1. The interceptor
    - It allow us to intercept when the local system make the call to the object and invoke the same function on the remote object.     
    - The using interceptors in adaptive middleware
        - We could use an interceptor to support mobility. In that case, a request-level interceptor would first look up the cur- rent location of a referenced object before the call is forwarded. 
        - Likewise, an interceptor can be used to transparently encrypt messages when security is at stake. Another example is when logging is needed. Instead of letting this be handled by the application, we could simply insert a method-specific intercep- tor that would record specific events before passing a call to the referenced object. More of such example will easily come to mind.  

2. The self-*
    - It is the automatic adaptivity. 
    - The star represent generic and can be anything.                    
    - We do not want the user to manually intervene to get the system to work.        
    - The self-configuration, it automatically decide that what will install.
    - The self-managing, if the system is disconnected, it will have application to recognize it automatically.   
        - In the unstructured P2p system, the nodes exchange membership information.    
    - The self-healing, if the system is disconnected, it will have application to recognize it automatically.      
    - The self-optimizing, the memory monitor, if the load is high, it will scale up to reduce the load on the server.      

3. The difference between the process and the thread
    - The process is seperated in memory.
    - The thread inherently share memory.      

4. The thread exist inside of the process.      

5. The process can fork process and can fork thread.      

6. The thread can fork thread, but it is not going to fork process.      

7. The processor context
    - It is the value stored in the registers of the processor.       

8. The thread context
    - It inherently contain the processor context.  
    - It contain the thread state.      
        - The sleep state
        - The active state
        - The wait state

9. The process context contain the thread context. The thread context contain the processor context.      

10. The context switching
    - The proccess switching is expensive.      
    - The creating and destroying thread is cheap.      

11. In the concept of context switch the problem
    - The OS will perform the context switch whenever it want.

12. The concurrency issue
    - The two thread run correctly, but the context switch allow us to have two successful increment operations that have resulted in the value only equaling one more than it did when we start.      
    - We will use lock to ensure it will not happen.      

13. The advantage and disadvantage of thread
    - The advantage, it is cheap and parallel.
    - The disadvantage, it is run concurrently.      

14. The thread safe
    - Thread safe API, API itself will prevent concurrency issuefrom happening. We do not have to worry about locks, critical sections, and mutexes.      

15. The thread safe data structure

16. The concurrent
    - The programming, program has multiple threads that are working on the same piece of data.
    - The execution, when multiple processes are swapping on and off a single core processor.    
        - The context switching.              
    - The computing, multiple processes that are running simultaneously on different cores. There are different processors, which do not interact.      







1. The user level thread
    - The implementation is efficient.     
    - If one thread is blocked, the entire process will be blocked.

2. The kernel level thread
    - You can pick one thread to run. The kernel can clock one thread and run the other thread.      
    - It is not efficient.             

3. The lightweight process is not the thread.     
    - Statically associating only a single thread with a lightweight process is not such a good idea
        - In this scheme, we effectively have only user-level threads, meaning that any blocking system call will block the entire process. 

4. The multithread 
    - The hiding network latency.       
        - If you go to the website, it will download something, the web browser will fork the thread for every HTTP request. 
    - The multiple thread from multiple machine. It is doing multiple things at once.      

5. The thread is cheaper and easier than the process.        

6. The virtualization
    - The hardware change faster than the software.      
    - The ease of **portability**, **scalability**, and **migration**.              
    - The isolation of failing and attack.      
    - The virtualization is easier than the code migration.             

7. The virtualization level
    - The hardware.
    - The operating system.
        - The API.
    - The library.

8. The process VM and the VM monitor
    - The process VM present the uniform environment to the application.        
        - JVM, the Java code is not runnning on the operating system, it is running on the JVM.
        - The Java is portable. You make minimum number of modification for the code. The code will run somewhere else.      
    - The VM monitor, hypervisor    
        - It is running directly on the top of the hardware.
        - Make copy of the applications and the operating system to virtual machine monitor.       
        - Make copy of the server.                  

9. The client stub
    - It present the environment to the application. So the application does not have to make any specific programming adjustment.      

10. The transparency
    - The access transparency, hide the difference in data representation and the way of object accessed. We do not want our user to worry about how the data are represented. 
        - It handle at client side.
        - There are a lot image format. You go online to see the image. You do not care about what format the image was stored as. 
        - The directory structure on Unix and Windows are different. They have different location. They have different layout. We do not want our user to worry about that. 
    - Location transparency, hide where the object is located.
    - Relocation transparency, hide the fact that the object may be moved to another location when it is in use. 
    - Migration transparency, hide the fact that the object may move to another location.
    - Replication transparency.
        - It handle at client side. 
    - Concurrency transparency.
    - Failure transparency. 
        - It only happen at client. 
        - The server does not know if there is something wrong happen, it just goes down. So the first machine recognize the error is the client. So the client is the first system to identify that there is something wrong      

11. The server
    - The superserver
        - It listen to a few port, provide process to handle the request, and continue to listen.      
        - The superserver is providing multiple services to multiple incoming data stream. 
    - The iterative vs concurrent servers
        - The iterative servers handle one client at a time.
        - The concurrent server handle multiple client at a time.      

12. The out of band communication

13. The stateless and stateful servers
    - The stateless server
        - It will not maintain info about the client.
        - We do not track anything that the client did between the session.      
        - It does not store the info regarding what the client has done if the client get disconnected.      
    - The reason to use the stateless is that it is very easy to program. We do not have to do a lot of work with the state server.      
    - The client and server are independent
        - We cannot preemptively push you updated data.    
    - The stateful server
        - It allow user to download the data before user request it.
        - we are keeping track of the operation of the application on the client.             
        - It is not the surveillance.     
        - The user perceive that the performance increase. 
    - Is a server that maintains a TCP/IP connection to a client stateful or stateless?
        - Assuming the server maintains no other information on that client, one could justifiably argue that the server is stateless. The issue is that not the server, but the transport layer at the server maintains state on the client. What the local operating systems keep track of is, in principle, of no concern to the server. 
    - Imagine a Web server that maintains a table in which client IP addresses are mapped to the most recently accessed Web pages. When a client connects to the server, the server looks up the client in its table, and if found, returns the registered page. Is this server stateful or stateless?
        - It can be strongly argued that this is a stateless server. The important issue with stateless designs is not if any information is maintained by the server on its clients, but instead whether that information is needed for correctness. In this example, if the table is lost for what ever reason, the client and server can still properly interact as if nothing happened. In a stateful design, such an interaction would be possible only after the server had recovered from a possi- ble fault. 

14. The TCP handoff
    - How TCP packets flow in the case of TCP handoff, along with the information on source and destination addresses in the various headers 
        - There are various ways of doing this, but a simple one is to let a front end to execute a three-way handshake and from there on forward packets to a selected server. That server sends TCP PDUs in which the source address cor- responds to that of the front end. An alternative is to forward the first packet to a server. Note, however, that in this case the front end will continue to stay in the loop. The advantage of this scheme is that the selected server builds up the required TCP state (such as the sequence numbers to be used) instead of obtaining this information from the front end as in the first scenario. 

15. The IPv6
    - The network address translation is that it allows us to split the single IP address into 65,000 independent addresses. The one IP address can be shared by 65,000 and change systems.      

16. The Mobile IPv6



1. The layer stack of the protocol
    - Application protocol
        - We build up the middleware below the application layer 
    - Presentation protocol
        - The access transparancy
        - The middleware
    - Session protocol
        - Track who is participating the communication session
        - It is the multicasting
        - The middleware
    - Transport protocol
        - We build up the middleware above the transport layer 
    - Network protocol
    - Data link protocol
    - Physical protocol

2. The middleware layer
    - The middleware provide the services for the autonomous systems participating in the distributed system.      
    - The communication protocol.
        - The marshaling
            - we would have demultiplexing in addition to identifying what all the components of this particular message are, how we are actually going to invoke that procedure or that method. 
        - The naming protocols 
            - How we find things.
            - The share resource.
        - The security protocols
            - The secure communication.
        - The scaling mechanisms
            - The replication and caching.

3. The types of communication
    - Transient vs persistent
        - Transient communication: Comm. server discards message when it cannot be delivered at the next server, or at the receiver.
        - The transient communications is that when you send data across a network, if the server is simply not ready for it, then the server will not get the data, so the data will disappear. The packet switch in the intermediate network will not store data. It will only forward the data.                 
        - Persistent communication: A message is stored at a communication server as long as it takes to deliver it. 
        - Persistent communication basically means that a message is stored at a communication server. And that message is going to be stored at that data Center for as long as it takes to actually deliver it to the targeted in system. 
    - Asynchrounous vs synchronous
        
4. We marshal the data for the request, we send the data across the network, it transmit the intermediate system and arrive at the server, it will complete the data request, and return the result to the client. 
    - Places for synchronization
        - The synchronize at request submission. 
            - The synchronization at request submission is a receipt and assurance that we have your message and we will attempt to deliver it. 
        - The synchronize at request delivery. 
        - The synchronize after request processing.

5. The intermediate network is packet switch and router. It will not store data. It will get the packet and forward it. 

6. The transient synchronous communication
    - Client and server have to be active at time of communication. 
    - Client issues request and blocks until it receives reply. 
    - Server essentially waits only for incoming requests, and subsequently processes them.
    - The disadvantage
        - The client can't really do anything while it's waiting for the server's response. 
        - Failures have to be handled immediately: the client is waiting. 
            - The failures transparancy. We want to hide failures from the user. And so it's our obligation to make sure that the user is not aware of the problem that is going on. 
            - The server does not know if there is something wrong happen, it just goes down. So the first machine recognize the error is the client. So the client is the first system to identify that there is something wrong. 
            - We want to hide failures from the user. And so it's our obligation to make sure that the user is not aware of the problem that is going on. 
        - The model may simply not be appropriate.
            - The human to human activity. 
7. The message oriented middleware
    - The persistent asynchronous communication
        - Processes send each other messages, which are queued. 
        - Sender need not wait for immediate reply, but can do other things. 
        - Middleware often ensures fault tolerance.
            - We don't expect an immediate response when we're using asynchronous communication, this is the benefit to fault tolerance. 
            - If there is something wrong for the asynchronous communication, we don't have to identify that immediately. 
    - What happens here is that we want messages to be queued so that the sender does not have to wait for a response from the client. And the client doesn't have to be running at the same time to actually receive that message from the sender. 

8. The Remote Procedure Call RCP
    - We don't have specific send and receive functions in our code. 
    - We want to make things look like the function call. So there is send and receive. There is only the function call. 
    - it does not matter that if it is running remotely or locally. 

9. The parameter marshaling
    - There’s more than just wrapping parameters into a message
        - Client and server machines may have **different data representations**.
            - The byte ordered in the memory. The endianness. 
        - Wrapping a parameter means **transforming a value into a sequence of bytes**.
        - Client and server have to **agree on the same encoding**:
            - How are **basic data values** represented.
                - The integers, floats, characters.
            - How are **complex data values** represented.
                - The arrays, unions. 
        - Client and server need to **properly interpret messages**, transforming them into machine-dependent representations.

10. The asynchronous RPCs
    - Try to get rid of the strict request-reply behavior, but let the client continue without waiting for an answer from the server. 

11. The message queue is not the data server. 

12. The data stored in the data server is the long term data. 

13. The message broker
    - The source client prepare the message queue, the source client will place the message into incomming queue at the message broker, the message broker will implement the access transparancy with the conversion rules.
    - The message broker is the intermediate system. It is not the file server. 

























1. The limit the number of thread 
    - First, threads require memory for setting up their own private stack. Consequently, having many threads may consume too much memory for the server to work properly. 
    - Another, more serious reason, is that, to an operating system, independent threads tend to operate in a chaotic man- ner. In a virtual memory system it may be difficult to build a relatively stable working set, resulting in many page faults and thus I/O. Having many threads may thus lead to a performance degradation resulting from page thrashing. Even in those cases where everything fits into memory, we may easily see that memory is accessed following a chaotic pattern rendering caches useless. Again, performance may degrade in comparison to the single-threaded case. 

2. The multithread is better than a single-threaded server and a finite-state machine server. When a single-threaded server might be better?
    - If the server is entirely CPU bound, there is no need to have multiple threads. It may just add unnecessary complexity. 
    - As an example, consider a telephone directory assistance number for an area with 1 million people. If each (name, telephone number) record is, say, 64 characters, the entire database takes 64 megabytes, and can easily be kept in the server’s memory to provide fast lookup. 

3. The concurrent server vs the multithread server
    - The advantage of the concurrent server is that separate processes are protected against each other, which may prove to be necessary as in the case of a **superserver** handling completely independent services.
    - The disadvantage of the concurrent server is that process spawning is a relatively **costly** operation that can be saved when using multithreaded servers. Also, if processes do need to communicate, then using threads is much cheaper as in many cases we can avoid having the kernel implement the com- munication. 

4. The strong mobility cannot be combined with executing migrated code in a target process. Give a counterexample
    - If strong mobility takes place through thread migration, it should be possi- ble to have a migrated thread be executed in the context of the target process. 


 
