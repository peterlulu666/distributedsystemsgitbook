## Quiz       

1. The synchronization
    - It is that putting stuff in order.      

2. Leap second
    - It is used to keep in phase with the sun.      

3. The Clock skew
    - One of those clocks is always going to be relatively fast. One of those clocks is only going to be relatively slow.      

4. The network time protocol NTP
    - It is going to query the system to see what time it is.      

5. The latency        
    - Anytime something travels across a network, it takes a nonzero amount of time to travel across a network.      
    - The latency is coming from three different instances here
        - When we have a client attempting to contact the server.      
        - The processing time.      
        - The return trip time.        
        
6. The absolute time and relative time
    - Absolute time is attempting to synchronize our systems with what the **world** thinks.      
    - Relative time is just attempting to synchronize our systems with **themselves**.        

7. The Berkeley Algorithm
    - The time daemon will send out the local time to other nodes in the system.        
    - The node answer.      
    - The time daemon tell others to adjust time.        
    - It is the relative time. It is not concerned with what the time is in the **world**. It's just concerned about the relative time in node **themselves**.        

8. The UTC and TAI
    - The UTC
        - It is the gold standard. It is the universal metric. We add the leap seconds to TAI and we have UTC. So the UTC is the TAI with leap Seconds added to it to adjust it.        
    - The TAI        
        - It is International Atomic Time. 

9. We cannot set a Clock backwards and we can only set a Clock forwards. 
    - We cannot receive it before we send it.           
    - We cannot make the time go faster. We can add 1 to the record value.      

10. The logical clock
    - The happen before relation
        - We cannot make the time go faster. We can add 1 to the record value.      
        - We send from small value to big value. If send is big, add 1 to send. It is the receive value.        




























