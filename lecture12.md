## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1b5YnDSwVMZ_LziSJoYzfjN1LeaJeF6Ri/preview"></iframe>

## Lecture Video



<iframe src="https://drive.google.com/file/d/1m7uI2s927SVJThXnwlIlSRdqirSdwtrw/preview" width="640" height="480"></iframe>

## Quiz        

1. The user level thread
    - The implementation is efficient.     
    - If one thread is blocked, the entire process will be blocked.

2. The kernel level thread
    - You can pick one thread to run. The kernel can clock one thread and run the other thread.      
    - It is not efficient.             

3. The lightweight process is not the thread.     
    - Statically associating only a single thread with a lightweight process is not such a good idea
        - In this scheme, we effectively have only user-level threads, meaning that any blocking system call will block the entire process. 

4. The multithread 
    - The hiding network latency.       
        - If you go to the website, it will download something, the web browser will fork the thread for every HTTP request. 
    - The multiple thread from multiple machine. It is doing multiple things at once.      

5. The thread is cheaper and easier than the process.        

6. The virtualization
    - The hardware change faster than the software.      
    - The ease of **portability**, **scalability**, and **migration**.              
    - The isolation of failing and attack.      
    - The virtualization is easier than the code migration.             

7. The virtualization level
    - The hardware.
    - The operating system.
        - The API.
    - The library.

8. The process VM and the VM monitor
    - The process VM present the uniform environment to the application.        
        - JVM, the Java code is not runnning on the operating system, it is running on the JVM.
        - The Java is portable. You make minimum number of modification for the code. The code will run somewhere else.      
    - The VM monitor, hypervisor    
        - It is running directly on the top of the hardware.
        - Make copy of the applications and the operating system to virtual machine monitor.       
        - Make copy of the server.                  

9. The client stub
    - It present the environment to the application. So the application does not have to make any specific programming adjustment.      

10. The transparency
    - The access transparency, hide the difference in data representation and the way of object accessed. We do not want our user to worry about how the data are represented. 
        - It handle at client side.
        - There are a lot image format. You go online to see the image. You do not care about what format the image was stored as. 
        - The directory structure on Unix and Windows are different. They have different location. They have different layout. We do not want our user to worry about that. 
    - Location transparency, hide where the object is located.
    - Relocation transparency, hide the fact that the object may be moved to another location when it is in use. 
    - Migration transparency, hide the fact that the object may move to another location.
    - Replication transparency.
        - It handle at client side. 
    - Concurrency transparency.
    - Failure transparency. 
        - It only happen at client. 
        - The server does not know if there is something wrong happen, it just goes down. So the first machine recognize the error is the client. So the client is the first system to identify that there is something wrong      

11. The server
    - The superserver
        - It listen to a few port, provide process to handle the request, and continue to listen.      
        - The superserver is providing multiple services to multiple incoming data stream. 
    - The iterative vs concurrent servers
        - The iterative servers handle one client at a time.
        - The concurrent server handle multiple client at a time.      

12. The out of band communication

13. The stateless and stateful servers
    - The stateless server
        - It will not maintain info about the client.
        - We do not track anything that the client did between the session.      
        - It does not store the info regarding what the client has done if the client get disconnected.      
    - The reason to use the stateless is that it is very easy to program. We do not have to do a lot of work with the state server.      
    - The client and server are independent
        - We cannot preemptively push you updated data.    
    - The stateful server
        - It allow user to download the data before user request it.
        - we are keeping track of the operation of the application on the client.             
        - It is not the surveillance.     
        - The user perceive that the performance increase. 
    - Is a server that maintains a TCP/IP connection to a client stateful or stateless?
        - Assuming the server maintains no other information on that client, one could justifiably argue that the server is stateless. The issue is that not the server, but the transport layer at the server maintains state on the client. What the local operating systems keep track of is, in principle, of no concern to the server. 
    - Imagine a Web server that maintains a table in which client IP addresses are mapped to the most recently accessed Web pages. When a client connects to the server, the server looks up the client in its table, and if found, returns the registered page. Is this server stateful or stateless?
        - It can be strongly argued that this is a stateless server. The important issue with stateless designs is not if any information is maintained by the server on its clients, but instead whether that information is needed for correctness. In this example, if the table is lost for what ever reason, the client and server can still properly interact as if nothing happened. In a stateful design, such an interaction would be possible only after the server had recovered from a possi- ble fault. 

14. The TCP handoff
    - How TCP packets flow in the case of TCP handoff, along with the information on source and destination addresses in the various headers 
        - There are various ways of doing this, but a simple one is to let a front end to execute a three-way handshake and from there on forward packets to a selected server. That server sends TCP PDUs in which the source address cor- responds to that of the front end. An alternative is to forward the first packet to a server. Note, however, that in this case the front end will continue to stay in the loop. The advantage of this scheme is that the selected server builds up the required TCP state (such as the sequence numbers to be used) instead of obtaining this information from the front end as in the first scenario. 

15. The IPv6
    - The network address translation is that it allows us to split the single IP address into 65,000 independent addresses. The one IP address can be shared by 65,000 and change systems.      

16. The Mobile IPv6































