## Quiz        

1. In the context of streaming multimedia, what is the downside to streaming higher quality media?
    - It requires more throughput

2. Briefly describe the difference between names, identifiers, and addresses.  Of the three, which can an entity only have one of?
    - The name is finding something. We use it to differentiate the entity from other entity. We use it to denote entity.                
    - The identifier refer to entity. It refer to at most one entity. It is not shared. 
    - The adddress is the access point.
    - Of the three, which can an entity only have one of? 
        - It is the identifier.

3. In name spaces, what is the difference between an absolute path and a relative path
    - If the path name that start at root, it is the absolute path name. Otherwise, it is the relative path.

4. In a name space, what is the difference between an absolute path and a relative path?  Why must the name space graph be acyclic?
    - If the path name that start at root, it is the absolute path name. Otherwise, it is the relative path.
    - The acyclic is to prevent infinite loops.

5. What is the main difference between forwarding pointers and a home-based approach in naming?
    - The forwarding pointers is that when an entity move, it will go to the next location, once it gets there, you can go anywhere. You can leave behind a pointer to wherever you are at.        
    - In the home-based approach, the home address register at naming service. The home register the foreign address. The client contact home location and then the foreign location.

6. In the context of Content Distribution Networks, briefly describe the “enter deep” server position strategy.
    - It is that push the server deep into the network. So put server cluster in a lot of locations. We will push it physically close to the user.

7. What is the difference between data being received and data being delivered?
    - The data received is that the data has arrived at the receiver buffer, it does not mean it has been consumed.           
    - The delivered is that the data has arrived at the receiver buffer and it has consumed the data.        
    - It is possible that it is received, but not delivered.

8. What information is used by a process running on one host to identify a process running on another host?
    - The IP address and the port number.

9. In the context of transport services, give an example of an app that would require 100% data integrity.
    - The file transfer.

10. Can a packet still be dropped after it’s been received?
    - Yes.

11. Why might we want to move data physically closer to end users?
    - In order to make the connection faster. The response time is depending on the physical distance. The user get slow connection if they are not close to the data.

12. What do the root DNS servers track?
    - The copy of the top level domain server.

13. UDP is a “best effort” protocol.  What is meant by “best effort”?
    - It is the one effort. It try once.

14. Briefly explain the difference between forwarding and routing.
    - The forwarding is physicall taking data from one port on the router to other port on the same router.     
    - The routing is to determine the best path through the network.

15. Why could virtual circuit networks offer quality of service minimums?
    - They are able to guarantee resource at every hop along the way.

16. List the Quality of Service (QoS) minimums provided by packet-switched networks.
    - There aren’t any

17. In the context of TCP, what is the difference between “congestion control” and “flow control”?
    - The congestion control is ensure not to overwhelm the network.              
    - The flow control is ensure not to overwhelm the receiver.

18. We discussed two scenarios in class where using TCP may not be appropriate.  Briefly explain one of those scenarios.
    - If the medium is inherently lossy, we should not use the TCP. The TCP have the congestion control. When the TCP loses the segment, it assumes that it loses the segment due to the congestion. If you are running data over the network, the TCP is great for it. However, let's say that the medium is the air. The air is not the reliable medium. So the medium itself is dropping data. You are lossing segment due to the air. You are not lossing segment due to the congestion. However, the TCP does not know it. The TCP assumes that it loses the segment due to the congestion. So it will be stuck down.
    - If the medium is inherently lossy. We cannot use TCP. TCP have the congestion control. If TCP lose packet, it assume that it is due to the congestion. It is OK to run data over the network. It is not OK if the medium is the air. The air is not the reliable medium. It will drop data itself. You lose packet due to the air. It is not due to the congestion. TCP does not know that. It assume that is lose the packet due to the congestion. So it stuck down.

19. What is meant by “unstructured P2P network”?
    - Pick node at random.

20. Briefly describe the concurrent computing.
    - It is that multiple process are running at the same time on the different core. The process will not interact.

21. There are generally three levels of context.  Name them and give a brief description of them.
    - The thread context. It contain the processor context and it contain thread state.
    - The process context. It contain the thread context. It is like memory mapping.     
    - The processor context. It contail the stack pointer, program counter, and the page table register. It is the value stored in the register of the processor.

22. Why can RPCs not simply pass-by-reference?
    - It has to convert between different data type and different units of measure. So it cannot simply pass-by-reference.

23. What is meant by an API declaring a data structure “thread-safe”?  If a data structure is thread-safe, are all the data in the data structure also thread-safe?
    - It is that the API itself can prevent concurrency issue from happening. You do not worry about locks, critical section, and mutexes.
    - No.

24. We discussed two scenarios in class where using a process might be preferable to using a thread.  Briefly explain one of these scenarios.
    - If you will prevent it from running concurrently, the process is preferable to the thread .
    - In the concurrent server, there are seperate process that are protected against each other, which may be necessary as in the case of the superserver handling completely independent service. So the process is preferable to the thread.

25. List the values in the finger tables of each active node in this DHT.  Please list them in the format:
    - Node _: #, #, #
    - <img src="img/test 1.jpg" alt="test 1" width="500" height="600">
    - 1_:3,3,5，顺时针和1距离1是2, 2 is not available, pick 3. 顺时针和1距离2是3, pick 3. 顺时针和1距离4是5, pick 5.
    - 3_:4,5,7 
    - 4_:5,6,1 
    - 5_:6,7,1 
    - 6_:7,1,3 
    - 7_:1,1,3
    - <img src="img/The finger table.png" alt="The finger table" width="500" height="600">
    - Node 0: 顺时针和0距离1，顺时针和0距离2，顺时针和0距离4



























