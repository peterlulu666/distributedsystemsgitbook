## Quiz        

1. Causal consistency
    - It is the distributed shared memory protocol

2. The data store is considered causally consistent if it obeys into the following conditions
    - Write that are potentially **causally related**
        - Have to seen in the same order by all of the processes
        - In the same order
    - The **concurrent writes**
        - It can be seen in any order on different machines

3. The concurrent
    - It is that it happens simultaneously and it does interfere with one another or it does have an impact on individual threads
    - In the causal consistency, it is that it happens simultaneously and it does not have an impact on individual threads
    - If they are concurrent, we can see them in any order

4. In the Vector clocks, the **causally related** is that it is possible that the two events had an impact on one another

5. If two things are not causally related, it is to say that there is no potential that the two events had an impact on one another

6. There is no timing in the causal consistency

7. The grouping operation
    - The mutual exclusion
        - We use it to control access to shared data
    - The synchronization variables
        - When we are sharing data in between processes or threads, we use the synchronization variables
        - It is the lock
    - The critical section
        - It is the portion of code that is accessing the data that shared between multiple thread
    - Each object have its own lock. Each synchronization variable is owned by the process that last required it. If you want to operate on that piece of data, you have to have the synchronization variable
    - If you acquire the synchronization variable, any changes the prior process made to that data are going to be shown to you
    - If you you write to the synchronization variable, you lock all other processes out so that they can't even read what is contained in that critical section. It is the exclusive mode access
    - The exclusive mode access
        - Only that process can read and write to that data. No other process could even read that data. Once you are done, you release the lock so that other processes can acquire it
    - Acq(Lx) acquire the lock to x

8. In the mutual exclusion, if you do not release the locks then we're going to have problem

9. The eventual consistency
    - It is pushing the updates out systemwide gradually and we're not updating everything immediately
    - It is working with the simultaneous updates and running strong consistency model
    - If the system makes an update to the particular data store and as long as it is accessing the same data store, then it's not necessary for the data store to push the updates anywhere else in the network
    - If the system change the location and access the different data stores, then there is problem with the consistency. The previous data store has not pushed updates out elsewhere in the network. So the updates are not in the new data store
    - the problem is the location transparency

10. The client centric consistency
    - It is that instead of keeping the entire system consistent and up-to-date, we're just making the system appear to be consistent to one client

11. The monotonic read
    - It is that if a process reads a data item, any successive read by the process on the data item will return either the value that it saw earlier or a more recent value

12. The monotonic write
    - It is that a write operation by a process on a data item have to be completed before any successive write operations on this data item by the same process
    - Let's say that you update a replica. You have to update the first commit before you commit the successive writes at the replica

13. The read your write
    - The effect of a write operation by a process on data item will always be seen by a successive read operation on this data item by the same process
    - All write operations have to be completed before any successive read operations
    - It applies to one individual process only, not the entire system as a whole
    - It is like update password
    - It is like you update webpage. You've written to that data, but when you read what you just updated, you don't see it initially

14. The writes follow reads
    - It is that if a process reads a value from the write and then later creates its own write, then everyone has to see the 1st write before the 2nd write

15. The replica server placement
    - The dot is like the user on the map. You recognize density of users and you place your servers. You do not want the density to be small or large
    - We cannot build the data center infrastructure anywhere we want. We rely on big company to build that type of infrastructure for us

16. We have three different types of replicas
    - The permanent replicas
        - It is the systems where we initially put all of our data
        - It is expected to be in the same place as long as as you have that particular data
    - The server initiated replicas
        - It is the copies of the data store that exist to enhance performance and they are created at the initiative of whoever owns that particular data store
        - It is like we push those replicas out, and they can scale up
        - It can scale down
            - When the scale decreases when those queries coming into the hits on that file decrease below a certain threshold, then you delete the replica, you scale back down
    - The client initiated replicas
        - It is the caching






































    
