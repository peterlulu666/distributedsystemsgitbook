## Lecture        

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1TI3Vlel2UN797P1DYRdFrRrsQWNFMCv_/preview"></iframe>

## Lecture Video



<iframe src="https://drive.google.com/file/d/1C0RzYE1Hfm7FUkIkmjbvYlgaXvlhur2x/preview" width="640" height="480"></iframe>

## Quiz        

1. Hash Functions
    - Hash functions produce a fixed length output for
every input
        - There is one output for every input
    - One way hash functions produce output that cannot
be used to figure out the input
        - We cannot use the output to identify what input generated the output
    - Hash functions produce the same output for the
same input 
    - Cryptographic hash functions should exhibit collision
resistance
    - The hash function is not the encryption
        - The **encryption** is that if you use the encryption algorithm to create the output, the output can be decrypted. The one way hash function cannot use the output to identify what input generated the output

2. Authentication Based on a Shared Secret Key
    - The authentication protocol based on a secret key
    - Build up the secure channel with the KAB. The KAB is the **shared** secret key
    - The challenge response protocol
        - It is that you have a challenge question that can only be answered correctly if you have the shared secret key
    - RA, RB, it send challenge
    - KAB, it respond the encrypted challenge
    - <img src="img/Authentication Based on a Shared Secret Key.png" alt="Authentication Based on a Shared Secret Key" width="500" height="600">
    - It reduced it from five to three
    - <img src="img/It reduced it from five to three.png" alt="It reduced it from five to three" width="500" height="600">
    - It can be easily be defeated by the reflection attack
    - The mistakes made during the adaptation of the original protocol was that the two parties in the new version of the protocol were using the same challenge in two different runs of the protocol
    - <img src="img/the reflection attack.jpg" alt="the reflection attack" width="500" height="600">
    - The obfuscation is that it try to hide the detail
    - The nonce is a random number that is used only once
    - The **designing** security protocols that do what they are supposed to do is often much **harder** than it looks
    - The **tweaking** an existing protocol to improve its performance, can easily affect its **correctness** as we demonstrated above

3. Authentication Using a Key Distribution Center
    - The Key Distribution Center
        - It is in the institutional network. It is to generate symmetric keys that two or more workstations can use
    - The pro
        - Each workstation is only required to have one key. We manage less key
    - <img src="img/Key Distribution Center.png" alt="Key Distribution Center" width="500" height="600">
    - The problem
        - It is going to initiate communication with Bob. Opening up a secure channel on the KDC cause the scalability problem





























