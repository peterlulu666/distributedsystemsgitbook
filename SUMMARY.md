# Summary

* [Introduction](README.md)
* [Lecture 1 Networked applications, sockets, transport services](lecture1.md)
* [Lecture 2 Web browsing, FTP, e-mail](lecture2.md)
* [Lecture 3 DNS, peer-to-peer overview](lecture3.md)
* [q1](q1.md)
* [Quiz 1](quiz1.md)
* [Lecture 4 Transport services, reliable versus unreliable protocols](lecture4.md)
* [Lecture 5 TCP](lecture5.md)
* [Lecture 6 Host-to-host communication, IP addressing, network partitions](lecture6.md)
* [Lecture 7 Goals of distributed systems, transparency, scalability](lecture7.md)
* [Lecture 8 Scalability challenges, false assumptions, types of distributed systems](lecture8.md)
* [q2](q2.md)
* [Quiz 2](quiz2.md)
* [Lecture 9 Transactions, pervasive systems, architectural styles](lecture9.md)
* [Lecture 10 Application layering, hybrid architectures, CDNs](lecture10.md)
* [Lecture 11 Adaptive middleware, processes versus threads, concurrency](lecture11.md)
* [Lecture 12 Virtualization, code migration](lecture12.md)
* [Lecture 13 Types of communication, RPCs, message brokers](lecture13.md)
* [q3](q3.md)
* [Quiz 3](quiz3.md)
* [Lecture 14 Streaming multimedia](lecture14.md)
* [Lecture 15 Intro to naming and name resolution, DHTs](lecture15.md)
* [Lecture 16 Network proximity, HLS, name spaces](lecture16.md)
* [t1](t1.md)
* [Test 1](test1.md)
* [Lecture 17 Synchronization, clocks](lecture17.md)
* [Lecture 18 Vector clocks, permission-based MUTEX](lecture18.md)
* [Lecture 19 Token-based MUTEX, elections, intro to consistency](lecture19.md)
* [Lecture 20 CONITs, sequential consistency](lecture20.md)
* [Lecture 21 Causal consistency, client-centric consistency, scalability and replicas](lecture21.md)
* [q4](q4.md)
* [Quiz 4](quiz4.md)
* [Lecture 22 Update operations, primary-based protocols, intro to fault tolerance](lecture22.md)
* [Lecture 23 Failure models, Byzantine fault tolerance, client responses](lecture23.md)
* [Lecture 24 Exactly-once semantics, orphaned operations, reliable multicasting](lecture24.md)
* [Lecture 25 Virtual synchrony, distributed commit](lecture25.md)
* [Lecture 26 Recovery, intro to security, Globus](lecture26.md)
* [q5](q5.md)
* [Quiz 5](quiz5.md)
* [Lecture 27 Focus of control, intro to crypto](lecture27.md)
* [Lecture 28 One-way hash functions, authentication with symmetric keys](lecture28.md)
* [Lecture 29 Signatures, Kerberos, certification authorities](lecture29.md)
* [q6](q6.md)
* [Quiz 6](quiz6.md)
* [t2](t2.md)
* [Test 2](test2.md)




























