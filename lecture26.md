## Lecture      

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1GyA3j01iQT7j54BV4tPn5FCZNac65vwb/preview"></iframe>

## Lecture Video



<iframe src="https://drive.google.com/file/d/1m1bSxoZSu5mwVvgRshXfqmwsZs8NtAdH/preview" width="640" height="480"></iframe>

## Quiz        

1. The recovery
    - When the process crashes and it comes back online. We take remedial action to make it consistent with all the other processes in the group
    - The forwards recovery
        - It is that we attempt to construct a new correct state
    - The backwards recovery
        - It is that we have to roll the state of the entire system back to the state that is correct
        - It is that we have to roll every process in this system back to a state that all of those processes acknowledged was correct and agreed was correct

2. The **checkpointing**
    - The **checkpointing** and **logging** are two different things
        - The **log** is that you keep track of the record that something happened
        - The **checkpointing** is that we save the system state. If we save the system state, it is written to the non volatile storage. It take up a lot of space and it take up a lot of time

3. The recovery line
    - It is the line connecting the two checkpoint

4. The independent checkpointing
    - It removed the overhead of synchronization
    - Instead of both process have to synchronize when taking checkpoint, it is that each processes take checkpoints independently at any time they feel appropriate
    - The problem
        - The **recovery lines** can only be **established** with **synchronization**. There is **no synchronization**. So there is **no recovery line**. If it failed with the **independent checkpointing**, it is going to roll back to its most recent **checkpoint**. However, there is **no recovery line**. So we have to **roll back** from checkpoint to checkpoint until we find a **consistent state**

5. The domino effect
    - If there is no synchronization between both processes

6. The security
    - The confidentiality
        - It is what you want to keep to yourself
    - The integrity
        - It is that your data is not changed when you do not want to change it
    - The access
        - It is when you choose to get the resources you have the right to access

7. The security threat
    - The interception
        - If you are sending data to another party, no unauthorized third party should be able to get to that data or view that data
        - The eavesdropper attacks
        - The man in the middle attacks
        - It is that the data is copied without permission
    - The interruption
        - It is that the data is corrupted and lost
    - The modification
        - It is that the unauthorized changes to your data
        - You change the program and data that is not supposed to be changed
    - The fabrication
        - It is when you create the data that is not supposed to be exist

8. The neutralize threat frustrate threat
    - The encryption
        - It is that we are transforming data into something the attacker cannot understand. But it can be transformed back into something that the authorized entity can understand
    - The authentication
        - It is how we verify who is the entities
    - The authorization
        - It is ensure that an entity is supposed to have the permission to perform requested action
    - The auditing
        - It is the analysis the vulnerabilities on the code
        - The proactive
            - It is that hire people to break into the systems in order to learn about the vulnerabilities
        - The reactive
            - The system have been hacked and you hire people to repair the system. You prevent the hacker to attack the system in the future

9. The globus security
    - It support large scale distributed computation. There is file sharing and other collaborative features built into it
    - The environment consists of multiple administrative domains
    - The local operations are subject to a local domain security policy only
        - The local operation implement their own authentication
    - The global operation require that the initiator to be known in each domain
    - The mutual authentication is in different domain 
    - The controlling access to resource is subject to the local security policy only
    - The user can delegate right to process
        - The user can allow the process to act on their behalf
    - If a group of process that are working on the same domain, they should be able to share credential
        - If you fork another process, you are supposed to grant the credential
    - The globus allow domain owners to focus on security solutions that are right for them
    - The globus is focusing the security only if your operation involves more than one domain


10. The globus representatives
    - The user proxy
        - The user proxy is a process that is given permission to act on behalf of a user for a limited period of time
            - You don't want to give someone rights to do something forever. It is for security policy
    - The resource proxy
        - The resource proxy is a process running inside of a domain that will translate global operations into local operations
            - When you make the translation and check the commands against your local security policy to make sure that that user that is attempting to execute this particular command is authorized to execute this particular demand.

11. The globus protocol
    - The first protocol dictates how a user creates a user proxy and how that user is going to **delegate permissions** to that user proxy
        - The **delegating permissions** making sure the proxy has the same permissions that you would have in that system. It is the **credential**
    - The second protocol deals with how a user proxy can request the allocation of resources in the remote domain
        - The user proxy tell the resource proxy to create process in the remote domain. Then the process represent user in the remote domain. The resource proxy check incoming request against local security policy and verify the user identity
    - The third protocol dictates how a user proxy request resources in other domains
    - The fourth protocol dictates how the user make it known to the remote domain





















    




















