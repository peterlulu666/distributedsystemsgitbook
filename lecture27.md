## Lecture        

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/10_N8gY3UwtjSiejQM6xtOxBXYoYvj4E9/preview"></iframe>

## Lecture Video



<iframe src="https://drive.google.com/file/d/1ZKOsvsNpQ3m3QbhiDewJM4pK38o0jaDd/preview" width="640" height="480"></iframe>

## Quiz        

1. The control
    - We build up the API to access the interface of the system. We limit parts of that API. We provide access API to certain users

2. Make data in that class private
    - It is not able to access the data and change the data in the class
    - The getter and setter

3. The layer of security
    - The physical layer
        - The remote system cannot hack the local system if it physically cannot get to it
    - The data link layer
        - We can implement both white and black listing based on Mac address
            - The Mac address
                - The Mac address is a value that is burned onto the network interface card and it is on the read only memory chip
                - You can use password to protect the Wi-Fi and you can set the router to allow the certain range of Mac addresses
                - The white list
                    - It is explicitly allowing things to access
                - The blacklist
                    - It is explicitly disallowing things to access
    - The network layer
        - We can write in Blacklist IP addresses
    - The transport layer
        - It build up the secure end to end tunnel on the transport layer. The data is encrypted at one system and the data can only be decrypted at the other system

4. The security and the trust are two different things
    - The security is the mechanism and technical
        - The security is the policy that dictates how the system is going to be used. We can build up the implement control over some aspect of systems that we have control
        - If it leave the system, there is no control
    - The trust is the emotional consideration
        - We have to trust certain systems that we do not have control. We have to trust things at certain level
    - The Trusted Computing Base
        - A TCB is the security mechanisms that enforce a security policy. It is going to be trusted
        - The small TCB is better

5. Cryptography
    - The plaintext. It is the arbitrary data
    - The ciphertext. If you encrypt the plaintext, it is the  ciphertext. It is the data that are encrypted
    - The keys
    - The EKP is encrypt the plaintext using key
    - The DKP is decrypt the ciphertext using key
    - The type of the intruder
        - The eavesdropping
        - The modifying the message
        - The intruder inserts encrypted messages into the communication system. It make the receiver believe it is from the sender

6. Symmetric cryptosystems
    - The symmetric key
        - It is the shared key. The receiver and the sender use the same key to encrypt and decrypt the data
    - KAB. It is the shared key. It is the key shared by A and B
    - K+A. It is the public key of A. The RSA
        - We want anyone who communicate with us to have access to the key
    - K-B. It is the private key of A
        - We only keep the key to ourselves

7. The Data Encryption Standard DES
    - It is used for symmetric cryptosystem
    - The round 16
    - The permutation
    - It is to try to break up **patterns** in the data
        - There is high frequency letter. If you input the same patterns of characters through to the encryption functions, the encryption function would produce a similar output every single time
    - The encrypt analysis
        - It is to analyze ciphertext and establish patterns in the ciphertext. It is going to use the patterns to identify the plaintext that generated it

8. The Public Key Cryptosystems RSA
    - The pair of key
        - The public key and the private key
    - You share the public key with everyone
    - You keep the private key to yourself
    - If you **encrypt** the data with your **public** key, it can only be **decrypted** by your **private** key and vice versa































