## Quiz 1


1. What information is used by a process running on one host to identify a process running on another host? 
    - The IP address.      
    - The port.        

2. When does an IP address get updated across the entire Internet?
    - If the DNS TTL expired. It will get updated.      

3. Why might we want to move data physically closer to end users?
    - The connection is faster. The response time is depending on the physical distance. The user will get slow connection if they are not close to the data.      

4. In the context of transport services, give an example of an app that would require 100% data integrity.
    - The file transfer.      
    - The online game. If you click the button, your character will do the thing very quick.      

5. There are multiple problems with centralizing services.  Name two of them.
    - If we only have one server and there is something wrong the server, the service on the server will be stopped. We have to wait for the server to get back.
    - It cannot handle all the traffic at once. It will increase congestion.

6. What is meant by the term “transparency” with respect to distributed systems? Briefly describe an aspect of the traditional client-server paradigm that could be said to be transparent.
    - The details are going to be hidden from the user. 
    - Let's say that the always-on host. The system will not run forever, but the user will perceive it is aways on.      

7. What is the correlation between the number of IP addresses and the number of physical systems reachable by that IP address, on the Internet? 
    - It is the same IP address, does not mean that it is the same physical system. So the IP address will not always be the same, it will change. So there is no correlation.      
 
8. What is meant by “encoding” something in HTTP?
    - It is the string. The string has special structure and format.    
    - It is the string, the plain text with certain commands or keywords  

9. What range of port numbers constitutes “well-known port numbers”? 
    - 0 and 1023

10. Where is the local DNS server position in the DNS hierarchy?
    - It varies.      
 
11. What is the difference between infrastructure-based and proximity-based peer-to-peer networks?
    - The infrastructure-based is something like the phone using the cellphone tower. You can connect to other system in long distance.        
    - The proximity-based is something like laptop using bluetooth. Your system and the other system have to be in very close distance.

12. Is it possible to get reliable data transfer when using UDP?  If so, how?
    - Yes. The reliable UDP is possible. You can build it up into the application layer.

13. What do the DNS top-level domain servers track?
    - The authoritative DNS servers.        
 
14. Give a brief definition of flow control, in the context of TCP.    
    - The TCP will ensure that the sender doesn’t send more data than the receiver can buffer.
 
15. If a local DNS server has cleared its cache (i.e., it has just recovered from a crash), how does it locate one of the DNS root servers?
    - There are 13 root DNS server address. You can keep a list. 
 
16. What transport services does UDP provide?
    - Effectively none.
    - The UDP does not care about anything.      
 
17. What is meant by “self-scalability” in peer-to-peer networks?
    - The peer will come and leave the network at any time. The network will expand and shrink.  
    - The new peer join the network, the network is bigger. they will bring new server and new compacity. The peer leave the network, the network is smaller.    

18. Why is it said that FTP sends control information out of band? 
    - There are two TCP connections. They are the TCP control connection and the TCP data connection. They are there at the same time.      

19. What is meant by saying that data have been “delivered”?
    - The data delivered means that the data has arrived at the receiver buffer and it has consumed the data.        

20. What do the root DNS servers track?
    - The copy of the top level domain server.      




































