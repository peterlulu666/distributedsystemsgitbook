## Lecture        

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1hALyEwdK5vqd2bxTFSRsWXEJMR-yLT5o/preview"></iframe>

## Lecture Video



<iframe src="https://drive.google.com/file/d/1z8DZ9CjYJ9b2j3V7STD5bMlZs5bRAvCi/preview" width="640" height="480"></iframe>

## Quiz      

1. The RPC semantics in the presence of failure
    - The client cannot find the server
    - The request from the client to the service is lost
    - The server crash after receive the request
    - The reply from the server to the client is lost 
    - The client crashes after sending request


2. The crash sequence
    - Send a completion message, M
    - Print the text, P
    - Crash, C

3. The client reissue strategies
    - The never reissue, risk that the client never perform
        - The at most once
    - The always reissue
        - The at lease once, risk that the client perform more than once
    - You can reissue if the acknowledgment was never received

4. The crash sequence
    - M -> P -> C
        - The server send the acknowledgment, completes the operation, and then it crashes
    - M -> C -> P
        - The server send the acknowledgement, but before it can perform the operation it crashes
   - P -> M -> C
    - The server receive the operation request, performs the operation, but after it send the acknowledgement it crashes
   - P -> C -> M
    - The server receive the operation request, performs the operation, but before it send the acknowledgement it crashes

5. what happens if the **server** does everything **right**, but we have an **issue** on the **client** side
    - The server can do everything correctly, but for some reason there is no response. So the reply is lost
        - The omission failure
            - The server did everything correctly and then for some reason you know we had maybe a buffer overflow on that particular server, we just didn't get the actual reply out
        - The network error
            - The server do everything correctly. However, the network does not carry anything on our behalf. So there is the network error
        - The server responded correctly but it just responded outside of the expected time window. So it just does not respond fast enough

6. The orphaned operation 
    - It is an operation that has been performed by the server, but the client is not there to receive the results of that operation. So the client send request to a server. The server is able to complete the request and then the client crashed
    - How to handle the orphaned
        - The **orphan extermination**. You keep a log RPC of the operation on the non volatile storage. After you reboot, you check the log and then you identify an orphan and then you terminate it
        - The **reincarnation**. It is that the time is divided into sequentially numbered epoch. Every time the client reboot, it broadcast the new epoch to the system. The system is going to kill the unacknowledgment operation from the previous epoch
        - The **gentle reincarnation**. It is that when the new epoch comes in, instead of just proactively killing off all of the local operations, the server try to return the requests to the client. If it does not accespt the request or it does not remember having asked for the request, then kill the operation
        - The **expiration**. The server keep a timer for each RPC. While the timer is active, it send acknowledgements to the owner. It continue to try to return the  operation to the client until the timer expires. At the timer expires, it kill the operation

7. The reliable multicasting schemes
    - We have to make sure that everyone who is in the group is actually getting all of the messages. Is it in ordering
    - It is up to the developer of the application developer to actually build these schemes themselves
    - We have to deliver the same message to every single process in that group in order to ensure that we get the fault tolerance

8. The non hierarchical feedback control
    - Anytime the receiver send the negative acknowledgement. The receiver reply the node missed. Then all of the receivers are going to get the retransmission
    - The problem
        - The network delay
        - Any time the sender stop and update the receiver, the receiver is going to get the duplicate information. They have to identify the duplicate information and drop it. So the node is going to wait. Anytime we have system resource that is waiting and idle, we are going to avoid it

9. The hierarchical feedback control
    - It is the same scheme as the non hierarchical feedback control. But we are partitioning the groups. So every time if there is the negative acknowledgments, it does not get to the sender every single time
    - The problem
        - We have to build and maintain the subgroup tree. We have to ensure that the subgroup is in their own subnet
        - If the sender is active and we send a lot of data, then the coordinator have a big cach to keep track
    - The dividing groups to subgroups and having a level of redundancy inside the subgroups is common to fault tolerance





















    
    
    
    
    


    

