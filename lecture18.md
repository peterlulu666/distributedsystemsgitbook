## Quiz      

1. The causality
    - It is that one event potentially cause something else to happen.              
    - It is that one event potentially trigger another event.            
    - It is that one event is triggered resulted in caused raised another event to happen.              

2. The vector clocks        
    - It allow us to look at any two operations that have happened between any two processes.             
    - How we update every process in the network as to what our vector clock presently is?
        - If you send the data, you are going to send a copy of your vector clock. We exchange copies of our vector clocks as we send and receive data.        
        - <img src="img/VectorClock.png" alt="VectorClock" width="500" height="600">
        - <img src="img/VectorClockNote.png" alt="VectorClock" width="500" height="600"> 

3. The causally related
    - If all of the values in one vector is greater than or equal to all of the values in another vector, the two events are causally related.      

4. The centralized algorithm
    - The node 1 send request to coordinator. The coordinator check if the queue is empty. The coordinator send the explicit OK.        
    - The node 2 send request to the same resource. The coordinator is not going to send back anything. It is the explicit wait.      
    - The node 1 release resource and tell the coordinator. The coordinator send the explicit OK.      
    - The coordinator
        - We use algorithm to build up a set of criteria to establish in ordering between all of the processes in the network.        
        - The coordinator job is to look at all of these access requests coming in for this particular shared resource.      
    - The deadlock
        - It is that every process in the system is sitting around and waiting and nobody is going to run.            
        - There is a risk of deadlock anytime we have a permission based solution.      
    - The no reply
        - Instead of forcing the coordinator to send the explicit wait messages every single time it gets a request, it just doesn't send anything. So if that node does not get anything, it assume it is wait.        
        - It reduces overhead.        
            - If you want to reduce overhead, you want to make your algorithms more efficient, consume less resources, and less fault tolerant.        
            - If you want to make your system be more robust, more fault tolerant, you are going to have more overhead.        
            - If the overhead simplifies the algorithm, the overhead is actually worth the effort.        

5. The distributed algorithm
    - It is that there's not one node that is making the access decisions for everybody.        
    - If receiver is not accessing, it send the explicit OK.        
    - If receiver have access, it does not reply and it queue the request.        
    - If receiver is going to access, it compare the timestamp and choose the lowest one.        
    - If the overhead simplifies the algorithm, the overhead is actually worth the effort.      

























