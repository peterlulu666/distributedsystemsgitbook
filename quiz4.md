## Quiz        

1. In the context of vector clocks, are the events at time (4, 9, 3) and (1, 0, 1) causally related?  If two events are causally related, does that guarantee that one event triggered the other?
    - Yes
        - If all of the values in one vector is greater than or equal to all of the values in another vector, the two events are causally related
    - No
        - It is the potentially

2. The text presents a centralized algorithm for mutual exclusion.  In the presented algorithm, the coordinator does not send a reply to a node requesting to access the shared resource if another node is presently accessing the resource.  What is the rationale for not sending a reply message?  What is a potential drawback to not sending a reply message?
    - It does not send anything. It reduce overhead
    - It take a long time to identify it

3. What is the overall point of synchronization?
    - It is that puting stuff in order

4. When adjusting a clock, what action do we never take?  Do we change the rate the clock is running, or just the time the clock is set to?
    - We never set the clock backward
    - It is just the time the clock is set to

5. Why do local, physical clocks never run at exactly the same rate?
    - The size and shape of quartz crystals is not the same

6. In the two election algorithms we studied in class, what criteria is the algorithm using to choose the new coordinator? Why are we using that criteria?
    - The process ID
    - Every process will have a process ID and they can be compared directly to one another

7. What is the purpose of a conit?
    - We can use it to calculate the numerical deviation

8. The book describes two primary reasons for replicating data.  Give a brief description of each of them.
    - The first reason is that it increase the reliability. It is possible that the server fail. If there are data spread out in more place, then we have copy
    - The second reason is that it increase the performance. It scale in number and in geographic area. If we have the copy of data, we can push data closer to user. The user is going to perceive the performance increase

9. In the context of grouping operations, what is the difference between “exclusive mode” and “nonexclusive mode”?
    - The exclusive mode is that only the process can read and write to the data. Other cannot. Once you are done, you release the lock and other can get it
    - The nonexclusive mode is that the processes that do not hold the lock can only read the locked variable

10. What is a consistency model?  Do consistency models correct inconsistencies as soon as inconsistencies are detected?
    - It is the agreement between the process and the data store
    - No

11. Give a brief definition of monotonic-write consistency.
    - It is that a write operation by a process on a data item have to be completed before any successive write operation on this data item by the same process

12. Eventual consistency operates with minimal overhead provided the client does what?
    - Continues to access the same replica. So it does not move

13. What is the major drawback of increased replication?
    - The cost for maintaining the replication

14. Based on the below diagram of two replicas, fill out the values for the CONITs.  List them as: Vector Clock A: ___; Order deviation A: ___; Numerical Deviation A: ____; et cetera.
    - <img src="img/q4q14.jpg" alt="q4q14" width="500" height="600">
    - Vector Clock A: (15, 8)     Vector Clock B: (0, 9)
    - Order deviation A: 3    Order deviation B: 1    
    - Numerical Deviation A: (0, 3)    Numerical Deviation B: (3, 8)

15. Refer to the above diagram illustrating a sequence of operations under causal consistency.  Are 'W(x)b' and 'W(x)e' causally related, yes or no?
    - <img src="img/q4q15.jpg" alt="q4q15" width="500" height="600">
    - Yes

16. Refer to the above diagram illustrating a sequence of operations under causal consistency.  Are 'W(x)c' and 'W(x)e' causally related, yes or no?
    - Yes

17. Refer to the above diagram illustrating a sequence of operations under causal consistency.  Are 'W(x)a' and 'W(x)d' causally related?  If not, how would you update the diagram to make them causally related?
    - No. We have to show that how to update the process and how to exchange the copy

18. Refer to the diagram of Lamport's Logical Clocks.  Fill out the diagram, listing your answers as
    - P1:
    - P2:   
    - P3:
    - <img src="img/q4q18.jpg" alt="q4q18" width="500" height="600">
    - P1: 51, 57, 63
    - P2: 30, 40, 50, 60, 70, 80
    - P3: 41, 49, 57, 65
