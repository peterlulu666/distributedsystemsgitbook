## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1Dg8N2BgbI1azSyFUMKtP21lHovXtZO7z/preview"></iframe>

## Lecture Video

<iframe src="https://drive.google.com/file/d/1oxprWQa2B4I2ptrS6qMghWQD9HojWGPX/preview" 
width="640" 
height="480"
allowfullscreen="allowfullscreen"></iframe>

## Quiz

1. Aasynchronous Communication
    - People can send message to you at any time. You can read the message at your convenience. The sender and the receiver do not have to schedule a time to be online at the same time.      

2. Encode HTTP
    - It is the string. The string has special structure and format.      

3. HTTP is the stateless protocol
    - The HTTP server will not maintain info about the client.  

4. Cache
    - It is the copy of data.    
    - We want to keep the data as close as we can. 
    - Reduce response time.
    - Reduce traffic. 

5. Cookies
    - It allows HTTP to behave like it keeps state.      












