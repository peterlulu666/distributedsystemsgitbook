## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1-u0YQiAprDGEFnZM-gVj0glq3qvXXyvr/preview"></iframe>

## Lecture Video

<iframe src="https://drive.google.com/file/d/1l37Rd8BUJN7Tymfbu3VJccSf5ITost6w/preview" 
width="640" 
height="480"
allowfullscreen="allowfullscreen"></iframe>

<iframe 
width="640" 
height="480"
src="https://drive.google.com/file/d/1OBo2sqBJyTI1btIaeOHh2b1I3lPPsPUm/preview"></iframe>

## Quiz

1. The problem of central DNS service
    - If we only have one server and there is something wrong with the server, the service on the server will be stopped. We have to wait for the server to get back. 
    - It cannot handle all the traffic at once. It will increase congestion. 
    - Physical distance. It will not close to every user.      
    - Maintenance. It is not easy to be fixed.      

2. Data close to the user
    - The connection is faster. The response time is depending on the physical distance. The user will get slow connection if they are not close to the data center.        

3. peer to peer chunk
    - It will optimistically unchoke the peer and get started sending chunk. So the new peer will join it.        

4. The IP address update
    - IF the DNS TTL is expired, it will get updated.        

5. The recursive queries and iterative queries      
    - It is possible to use any one.        

6. The DNS clear cache
    - Use root DNS server.

7. What do DNS top level domain track
    - The authoritative DNS servers.      












