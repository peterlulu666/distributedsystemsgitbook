## Lecture        

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1UZiLizD0Kdj6fFQduGBEEfUT0i0pfrda/preview"></iframe>

## Lecture Video



<iframe src="https://drive.google.com/file/d/10tnKmQV_7svlXM5Cx2jn4loENh590OG8/preview" width="640" height="480"></iframe>

## Quiz        

1. The atomic multicasting
    - It is that ensure that either a message is going to be delivered to all of the processes that are in this group or it is not going to be delivered to any of the processes that are in this group
    - The receive and delivery is not the same thing
    - The TCP ensure that there is reliable and in order delivery

2. The virtual synchrony
    - It is that either everyone has to receive this message or we discard this message and pretend like that message was never sent
    - There is consistent state before we actually pass it up to the application. Everything is taking care of in the communication layer
    - The group
        - It is that either **everyone** receives and delivers the message or **no one** receives and delivers the message is determining who is in the group
        - The establishing who is in a **group** is going to be the **first step** to a lot of our **fault tolerance**

3. The message ordering
    - The unordered multicasts
        - We are not applying the specific sequencing. We just pick a sequencing and then we apply it everywhere

4. The message stability
    - It is that determining who has actually received a message
    - The communication layer is going to cache some of those messages and it is going to hold on to them until it verify that every process that is still in this group has received them
    - If it verify that every process in the group has received the message, then it is the stable message
    - Only stable messages are delivered

5. The flush message
    - The process sending the messages that is buffered. It is going to send the packets to everyone
    - Everyone is going to reply the confirmation message to the process that send the flush message

6. The distributed commit
    - We have to ensure that every node presently in the group can perform the operation or nobody in the group can perform the operation
    - This is handled by a coordinating and so there are definitely decentralized ways of applying distributed commit. The centralized is easier
    - The other process is the participants
    - All the participants successfully perform the operation or the operation is rolled back at all the participants

7. The one phase commit
    - The coordinator tell all the participants to commit
    - The probelm
        - If the participants cannot perform the operation, they do not have the ability to signal their inability to complete that operation
    - It cannot abort the operation that is not completed successfully everywhere
        - The abort
            - If one of the participants cannot follow through with the order and we have to roll back everyone

8. Every single transaction will end one of two ways. Either everyone agrees to perform the operation and commit it or we have to abort it and we have to roll back

9. The two phase commit
    - The coordinator is in the initialization phase
        - The coordinator enter the initialization phase. It let the participants to perform the operation. It send the vote request to every single participant. The coordinator is there to wait the participants to vote that they either can commit the operation or they cannot. After the participants vote, if every single participant vote that they can commit, then we can send the global commit command. If any participants vote that they cannot commit, then we can send the abort commit command
    - The participant is in the initialization phase
        -  The participant enter the initialization phase. If it receive abort, then we can send the abort commit command. If everyone vote that they can commit, it goes to the ready state. It is there to wait for the decision from the coordinator. If it is the global commit command, it go to the commit state. If it is the global abort command, it go to the abort state
        - The participant is possible to timeout when it is waiting for the vote request. If the participant timeout in this phase, then it is going to vote to abort
    - <img src="img/The two phase commit.png" alt="The two phase commit" width="500" height="600">

10. The three phase commit
    - <img src="img/The three phase commit.png" alt="The three phase commit" width="500" height="600">
    - <img src="img/The three phase commit state.png" alt="The three phase commit state" width="500" height="600">





























    






























