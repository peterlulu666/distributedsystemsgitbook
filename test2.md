## Quiz      

1. The book describes two primary reasons for replicating data.  Give a brief description of each of them.
    - The first, it increase reliability. It is possible the server fail. If there are data spread out at more location, we have copy
    - The second, it increase performance. It scale in number as well as geographic area. If we have copy of data, we can push data closer to user. So user perceive performance increase

2. In the context of grouping operations, what is the difference between “exclusive mode” and “nonexclusive mode”?
    - The exclusive mode, it is that only the process can read and write to the data. Other cannot do it. Once you done with it, you release lock and other can get it
    - The nonexclusive mode, it is that the process that do not hold the lock can only read the locked variable

3. Why do local, physical clocks never run at exactly the same rate?
    - The size as well as shape of quartz crystal is not the same

4. Give a brief definition of monotonic-read consistency.
    - It is that if a process read data item, any successive read by process on this data item return either the value that it saw earlier or the more recent value

5. In the context of vector clocks, are the events at time (4, 9, 3) and (1, 0, 1) causally related?  If two events are causally related, does that guarantee that one event triggered the other?
    - Yes
    - No

6. The text presents a centralized algorithm for mutual exclusion.  In the presented algorithm, the coordinator does not send a reply to a node requesting to access the shared resource if another node is presently accessing the resource.  What is the rationale for not sending a reply message?  What is a potential drawback to not sending a reply message?
    - It does not send anything. It reduce overhead
    - It take a long time to identify it

7. What is the overall point of synchronization?
    - It is to put stuff in order

8. Give a brief definition of monotonic-write consistency.
    - It is that write operation by process on data item have to be completed before any successive write operation on this data item by the same process

9. In two-phase commit: if the coordinator has crashed, and a participant is stuck in the READY state, why can it safely commit if it finds another participant in the COMMIT state?
    - It is there to wait for decision from cooordinator. If it is the global commit command , it goes to commit state

10. The book presents four security mechanisms.  Briefly describe the purpose of authentication.
    - It is how we verify who is entites. It is to prove you are who you say you are

11. What is meant by a “pull-based” approach to update propagation?
    - It is client base. Client drive sequence. Client poll server to check update

12. The book presents four security mechanisms.  Briefly describe the purpose of authorization.
    - It is authenticating identiy and granting right

13. Define ‘value failure’ and ‘state-transition failure’.  What category of failure do these two specific failures belong to?
    - The value failure is the server receive request as well as server respond to it and send data to client. However, it is not the data that client anticipating. The value of response is wrong
    - The state transition failure is that server receive data and respond to it. After that, the server deviated from the right flow of control
    - They are response failure

14. With “at least once” semantics, the user is guaranteed what?  With “at most once” semantics, the user is guaranteed what?
    - With “at least once” semantics, the user is guaranteed that the operation is executed correctly. It safely perform many times. The operation return the same thing
    - With “at most once” semantics, the user is guaranteed that you do not have duplicate operation

15. List, and give a brief definition, of the three categories of fault recurrence.
    - The transient fault recurrence. It is going to occur once and then it disappear. Therefore, the subsequent operation is correct
    - The intermittent fault recurrence. It is going to occur, and go away, and then reappear
    - The permanent fault recurrence. It is going to continue to exist until faulty component is replaced

16. Briefly describe the goal of an atomic multicast.
    - It is to ensure that either message is going to be delivered to all process in group or it is not going to be delivered to any process in group

17. What is meant by a “push-based” approach to update propagation?
    - It is that propagate update to other replica without replica having to request new data. Server proactively push update to client

18. Briefly describe the “extermination” strategy of addressing orphaned operations.
    - It is that you keep a log RPC of operation on non volatile storage. After reboot, you check log and then you identify orphan and then you terminate it

19. With regards to firewalls, why might you want to prevent traffic from leaving your network?
    - It is to protect data. It is to protect privacy. it is to prevent data leak. You want to stop data go from local system to other system

20. With regards to firewalls, why might you want to prevent traffic from entering your network?
    - It is to prevent unauthorized traffic go into network

21. Give an example of a security mechanism you could implement at the data link layer.
    - Implement white and black listing based on MAC address

22. What happens if a subject does not appear in an object’s ACL?
    - There is no permission

23. Give an example of a security mechanism you could implement at the physical layer.
    - Do not allow remote system physically connect to local system

24. If you do not trust a layer of the system stack (e.g., the TCP/IP protocol stack, plus the OS, middleware, and application layers), what must you do?
    - Implement security at higher layer

25. What is the purpose of the “initial permutation” in encryption algorithms?
    - break up pattern in input




