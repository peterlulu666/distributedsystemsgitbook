## Quiz        
1. What is meant by saying that data have been “delivered”?
    - The data delivered means that the data has arrived at the receiver buffer and it has consumed the data.      

2. What is an n-way unicast?
    - It is that the host send the copy of the same packet to multiple destinations.      

3. One of the ways reliable transport protocols detect errors is the expiration of timers. What is the result if the timer is set too short?
    - The problem is the premature timeout. It will indicate that there is problem when nothing is wrong.      

4. If we create our own transport protocol, how do we ensure it is compatible with the public Internet?
    - We don’t need to – the public Internet ignores the transport protocol.      

5. TCP has a range of bits in its header that are not used for anything.  Why is that unused field there?
    - It is for the later on update. It is for compatibility. It is the extra space for the developer.      

6. Why is TCP so aggressive with its congestion control?
    - The queue fills up very quickly. When it fills up, it gets started dropping data very quickly. Then it takes a long time to empty all the way. So the TCP is aggressive with its congestion control.      

7. True or False: Virtual circuit networks are connectionless services
    - False      

8. Why could virtual circuit networks offer quality of service minimums?
    - They can guarantee resources at every hop along the way.      

9. What is a network partition?
    - It is the failure of a part of the network. The fault tolerance.        

10. How does a host know what its default gateway is?
    - DHCP.      

11. What is the purpose of a routing algorithm?
    - Determine the least cost end to end path.      

12. Briefly explain the difference between forwarding and routing
    - The forwarding is physically taking data from one port on the router to another port on the same router.             
    - The routing is determine the best path through the network. The best path is the lease cost path. It is the end to end path.        

13. Give a brief definition of “policy” and “mechanism.”  Of the two, which should distributed system designers focus on and why?      
    - The policy is what the system will do and what the system will behave.             
    - The mechanism is the tool for user to change the middleware.              
    - The distributed system designers should focus on the mechanism. The user will determine the policy.        

14. There are multiple metrics to quantify “cost.”  Name two of them
    - The money.              
    - The energy.        

15. Explain “consistency” in layman’s terms, and give an example of what might lead to a system being inconsistent
    - The consistency is the agreement.            
    - If there is multiple files, if we update one file, then we will update other files. If we do not update the other files, it will lead to a system being inconsistent.        

16. What is the one goal that virtually all distributed systems share?        
    - To make multiple independent systems appear as one system.        

17. What is the one type of transparency that is sometimes impossible to fully implement?  Give a brief example of why that transparency may not be possible        
    - The failure transparency.        

18. What is meant by supporting the “portability” of applications?        
    - It is the concept that the application can be moved to a different platform to do the same thing with minimum number of modification.        

19. What is meant by calling systems in a grid “heterogeneous”?        
    - The systems are different.      
    - If two organization work together, they will assemble into the virtual network and share the info. They are owned by different organization and administered by different people, but they are on the same network.        

20. What is meant by calling systems within a cluster “homogeneous”?        
    - The systems are virtually identical.      
    - It will run by the single organization. A group of system in the local network.        



















































