## Quiz        

1. The permission based
    - You go to the network and request permission to access the shared resource.      
        - The centralized
        - The distributed

2. The token ring algorithms
    - We create a circular overlay network across all of our nodes.      
        - It is the unordered group of process.      
        - It is the logical ring.      

3. The starvation 
    - It is that we have one node in the network that is continually accessing the resources and is preventing other nodes from accessing the resource.      

4. The election algorithm
    - The bully algorithm
        - It is attempting to identify the node with the highest process ID in the group of nodes.      
        - The node with the highest process ID is the coordinator.      
        - If the downstream notes wait for a long time, the fault detection will determine that something is wrong and recove from the faults.      
        - We run into the halting problem.      
    - If you receive an election message and you do not have a higher process ID than the node that initiated the election, you do not respond. 
    - If you receive an election message and you have a higher process ID, you respond with OK.      
    - If within the period of time it does not receive an OK from anyone, then it's going to assume that it is the new coordinator. At the same time, it's going to send the broadcast message that it is the new coordinator. 
    - At each step, each one of these election messages is a broadcast.      
    - <img src="img/election algorithm bully algorithm.png" alt="election algorithm bully algorithm" width="500" height="600">      
    - <img src="img/bully algorithm.png" alt="bully algorithm" width="500" height="600">      

5. The election algorithm
    - The ring algorithm
        - If it's not responding, it is going to forward that election message to its immediate successor and it's going to put its node ID in that particular message. It is like the distributed hash table.      
        - Every node has a process ID, it is compareable to one another. And no matter where we make the comparison, we're always going to make the same decision based on the comparison.      

6. The consistency and replication
    - It is to make copies of things and to move those copies around to different systems and to operate on copies all at the same time. It is important to the distributed systems.      

7. The reason for the replication
    - It increase the **reliability**
        - It is possible that there is the servers fails, the network partition, or other thing goes wrong for some reason. If there are data spread out in more place and we have the copy somewhere else, then losing that one node is not big deal.      
    - It increase the performance
        - The scaling in numbers
        - The scaling in geographic area
            - If we can make a copy of the data and we can push the data closer to the end users, then that particular end user is going to perceive the increase of performance.        
    - Caveat
        - It increase the performance
        - The cost for maintaining replication        

8. The high consistency
    - The stock market

9. The consistency model
    - It is the agreement between the processes and data store.        
        - If the process accesses the data store, then the data that is pulled out of the data store have to adhere to whatever consistency agreement that we have established.      
    - The agreement determine the consistency.        
    - The consistency is not identical. It is the agreement.      




















