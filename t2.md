## Quiz        

1. In the context of vector clocks, are the events at time (4, 9, 3) and (1, 0, 1) causally related?  If two events are causally related, does that guarantee that one event triggered the other?
    - Yes
        - If all of the values in one vector is greater than or equal to all of the values in another vector, the two events are causally related
    - No
        - It is the potentially

2. The text presents a centralized algorithm for mutual exclusion.  In the presented algorithm, the coordinator does not send a reply to a node requesting to access the shared resource if another node is presently accessing the resource.  What is the rationale for not sending a reply message?  What is a potential drawback to not sending a reply message?
    - It does not send anything. It reduce overhead
    - It take a long time to identify it

3. What is the overall point of synchronization?
    - It is that puting stuff in order

4. When adjusting a clock, what action do we never take?  Do we change the rate the clock is running, or just the time the clock is set to?
    - We never set the clock backward
    - It is just the time the clock is set to

5. Why do local, physical clocks never run at exactly the same rate?
    - The size and shape of quartz crystals is not the same

6. In the two election algorithms we studied in class, what criteria is the algorithm using to choose the new coordinator? Why are we using that criteria?
    - The process ID
    - Every process will have a process ID and they can be compared directly to one another

7. What is the purpose of a conit?
    - We can use it to calculate the numerical deviation

8. The book describes two primary reasons for replicating data.  Give a brief description of each of them.
    - The first reason is that it increase the reliability. It is possible that the server fail. If there are data spread out in more place, then we have copy
    - The second reason is that it increase the performance. It scale in number and in geographic area. If we have the copy of data, we can push data closer to user. The user is going to perceive the performance increase

9. In the context of grouping operations, what is the difference between “exclusive mode” and “nonexclusive mode”?
    - The exclusive mode is that only the process can read and write to the data. Other cannot. Once you are done, you release the lock and other can get it
    - The nonexclusive mode is that the processes that do not hold the lock can only read the locked variable

10. What is a consistency model?  Do consistency models correct inconsistencies as soon as inconsistencies are detected?
    - It is the agreement between the process and the data store
    - No

11. Give a brief definition of monotonic-write consistency.
    - It is that a write operation by a process on a data item have to be completed before any successive write operation on this data item by the same process

12. Eventual consistency operates with minimal overhead provided the client does what?
    - Continues to access the same replica. So it does not move

13. What is the major drawback of increased replication?
    - The cost for maintaining the replication

14. Based on the below diagram of two replicas, fill out the values for the CONITs.  List them as: Vector Clock A: ___; Order deviation A: ___; Numerical Deviation A: ____; et cetera.
    - <img src="img/q4q14.jpg" alt="q4q14" width="500" height="600">
    - Vector Clock A: (15, 8)     Vector Clock B: (0, 9)
    - Order deviation A: 3    Order deviation B: 1    
    - Numerical Deviation A: (0, 3)    Numerical Deviation B: (3, 8)

15. Refer to the above diagram illustrating a sequence of operations under causal consistency.  Are 'W(x)b' and 'W(x)e' causally related, yes or no?
    - <img src="img/q4q15.jpg" alt="q4q15" width="500" height="600">
    - Yes

16. Refer to the above diagram illustrating a sequence of operations under causal consistency.  Are 'W(x)c' and 'W(x)e' causally related, yes or no?
    - Yes

17. Refer to the above diagram illustrating a sequence of operations under causal consistency.  Are 'W(x)a' and 'W(x)d' causally related?  If not, how would you update the diagram to make them causally related?
    - No. We have to show that how to update the process and how to exchange the copy

18. Refer to the diagram of Lamport's Logical Clocks.  Fill out the diagram, listing your answers as
    - P1:
    - P2:   
    - P3:
    - <img src="img/q4q18.jpg" alt="q4q18" width="500" height="600">
    - P1: 51, 57, 63
    - P2: 30, 40, 50, 60, 70, 80
    - P3: 41, 49, 57, 65



1. When a lease is active, how are updates propagated?  When a lease expires, how are updates propagated?
    - When it is active, the server proactively push update to the client. It is the push base
    - When it is expire, the client poll the server to see if there is any new data available. If yes, it retrieve. It is the pull base

2. What is meant by a “pull-based” approach to update propagation?
    - It is the client base. The client drive the sequence. The client poll the server to check the update
    - If the read to update ratio is low, it is efficient

3. In the context of invalidation notices, what is meant by “eager consistency”?
    - It is that we fetch new data as soon as we get the invalidation notices. We make it available to user before they request it. So user perceive less latency

4. What is meant by a “push-based” approach to update propagation?
    - It is that propagate update to other replica without the replica having to request the new data. So the server proactively push update to the client

5. Briefly describe the two consistency / update strategies a client can employ when it receives an invalidation notice.
    - You can fectch new data as soon as we get the invalidation notices. So it is the eager consistency. It is the preemptive.  We make it available to user before they request it. So user perceive less latency
    - You can wait until you perform the operation on the data. So it is the lazy consistency.  It is not the preemptive. It is not going to automatically fetch data. So user is going to perceive latency

6. Why is establishing group membership important in atomic multicasts?
    - It is that either the message be delivered to all process in the group or it is not delivered to any process in the group. So it is either eveyone or no one that deliver the message in the group. It is important to establish group and determine who is in the group
    - The replica can perform the operation only if they have reached agreement on the group membership

7. What client reissue strategy guarantees that an operation will be performed exactly one time?
    - There is no one

8. List, and give a brief definition, of the three categories of fault recurrence.
    - The transient fault recurrence, it occur once. Then it disappears. So the subsequent operation is right
    - The intermittent fault recurrence, it occur, go away, and reappear
    - The permanent fault recurrence, it continue to exist until the faulty component is replaced

9. Briefly describe the concept of “graceful degradation”.
    - If there is a lot of things going wrong, you can still get it to provide a few thing working with no problem

10. What category of failure may clients not be able to identify as a failure?
    - The arbitrary failure

11. What is one down side to the “reincarnation” strategy of addressing orphaned operations?
    - A network partition may prevent all systems from receiving the epoch notification

12. Briefly describe the “distributed commit” problem.
    - The problem is that either all participant successfully perform operation or it is rolled back at all participant. So we have to ensure that everyone can perform operation

13. Why are we concerned that an operation need be performed no more than one time?
    - In order to ensure no duplicate operation
    - In order to ensure the safety

14. How does a client determine why it did not receive a response from a server?
    - It is not able to

15. What is the purpose of three-phase commit (be specific)?  Are all versions of three-phase commit the same?
    - It improve the two phase commit in terms of recovery. It avoid blocking processes in the presence of crash
    - No

16. Define ‘value failure’ and ‘state-transition failure’.  What category of failure do these two specific failures belong to?
    - The value failure is that the server receive request. The server respond to it. The server send data to the client. However, it is not the data that the client is anticipating. The value of the response is wrong
    - The state transition failure is that the server receive the data. The server respond to it. Then the server deviated from the right flow of control
    - They belong to the response failure

17. The book presents four security mechanisms.  Briefly describe the purpose of authentication.
    - It is how we verify who is the entities
    - It is to prove you are who you say you are

18. The book presents four types of threats.  Briefly describe modification.
    - It is the unauthorized change to the data. So they change the data that is not supposed to be changed

19. The book presents four security mechanisms.  Briefly describe the purpose of auditing.
    - It is the analysis the vulnerabilities on the code

20. The book presents four types of threats.  Briefly describe fabrication.
    - Making illegitimate data appear legitimate









1. What is the purpose of the “initial permutation” in encryption algorithms?
    - Breaks up patterns in the input

2. If you do not trust a layer of the system stack (e.g., the TCP/IP protocol stack, plus the OS, middleware, and application layers), what must you do?
    - Implement security at a higher layer

3. Give an example of a security mechanism you could implement at the transport layer.
    - Build up the secure end to end tunnel. The data is encrypted at one system and only be decrypted at another system

4. Give an example of a security mechanism you could implement at the network layer.
    - We can write in blacklist IP address

5. Give an example of a security mechanism you could implement at the data link layer.
    - We can implement both white and black listing based on the MAC address

6. How is a subject’s identity encoded into a capability?
    - There is no identity that is associated with it

7. With regards to firewalls, why might you want to prevent traffic from entering your network?
    - You want to prevent the unauthorized traffic go into the network

8. There are three different focuses of control.  Name and briefly describe each.
    - To prevent the invalid operation. The API is there to control access and check the operation
    - To prevent the unauthorized operation. It check the API. It limit the API
    - To prevent the unauthorized user. It check who is the user. It provide API access to certain user

9. In general, there are two different types of firewalls.  What is the difference between the two?
    - Packet filter and application gateway

10. What happens if a subject does not appear in an object’s ACL?
    - They have no permission

11. How do entities prove their identities to Certification Authorities?
    - There is no mandated process

12. With regards to firewalls, why might you want to prevent traffic from leaving your network?
    - You want to protect the data and prevent the data leak. You do not want the data to go from the local system to the other system

13. Explain what is happening is happening on step 4 of the following diagram.
    - <img src="img/KDC Quiz.png" alt="KDC Quiz" width="500" height="600">
    - The key distributed center use the shared key to encrypt the challenge. The key is shared key. It is shared by A and KDC. The challenge contain the challenge RA1. It contian the identity B, so they know who it is. It contain the shared key KAB, so A is going to use it. It contain encrypted challenge KB,KDC(A,KAB,RB1), So A is going to send use it

14. What is the purpose of running the hash function of Alice’s computer?
    - <img src="img/Digital signature Quiz.png" alt="Digital signature Quiz" width="500" height="600">
    - It is going to produce the output and send it to Bob. It is her signature

15. Does the hash function ensure confidentiality?
    - No

16. What is the purpose of using Alice’s private key in this algorithm?
    - If you encrypt data with private key, it can only be decrypted by the public key. Bob is going to decrypt it using the public key. So if Alice use the private key, bob is going to know it is talking to Alice

17. Does using Alice’s private key ensure confidentiality?
    - No. Everyone has Alice's public key

18. How is Alice’s password transmitted across the network?
    - It is never transmitted

19. How is KA,AS stored on Alice’s workstation?
    - It is not stored
    - It is not stored in the work station. It is generated dynamically at the time input the password


