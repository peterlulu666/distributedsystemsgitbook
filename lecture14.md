## Lecture        

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1S9nDj0HUsSO78dpnhpSKLVkHInKdgUs7/preview"></iframe>        

## Lecture Video        



<iframe src="https://drive.google.com/file/d/17wIvXcVE0Kf3pbT24IMxZTTHQRex03q_/preview" width="640" height="480"></iframe>

## Quiz        

1. The streaming
    - You don't want to download the entire file before you begin to play it. 
    - You can play it before you download the entire file. 

2. The conversational
    - The voice over IP
    - The limit
    - The Skype

3. The streaming live

4. The streaming stored video
    - Video recorded, sent, amd received
    - Match original timing
    - Network delay is vary
    - Chanllenge
        - pause, fast forward interactivity
    
5. The delay
    - The jitter
        - We are going to send the video at the steady rate if possible and we have the fluctuation and delay.        

6. We use UDP for multimedia        
    - TCP has congestion control and TCP is aggressive with its congestion control        
    - UDP is there to help us to receive the data as quickly as possible        

7. UDP does not travel faster than anything else in the network, but the send rate can be high due to there is no congestion control        
    - Error recovery built into the application level        
    - The problem of UDP is that it may not go through firewall due to the security concern and the congestion concern                 

8. UDP can be broadcast        

9. Transporting things for streaming multimedia
    - Use TCP and HTTP      
    - The streaming multimedia content use TCP and not UDP        
        - you're going to break the video into chunks and you use the HTTP GET to request the chunk        

10. Transmit things over the Internet
    - Use the DASH
        - dynamic, adaptive streaming over HTTP        
        - Server
            - break the video into chunks        
            - every chunk is stored and encoded at different rate        
            - manifest file, provide URL for different chunk         
        - Client
            - mesure bandwidth
            - determine when, what encoding rate, and where
            - pushing the complexity towards the edge
                - The client can do the work faster. So we want the client doing the work. 
                - It make the developer's life easier. It is easy to program.     

11. Voice over IP VoIP
    - We have to keep the delay minimal to maintain the conversational aspect.      
    - The Internet is a best effort service. We have no control over latency. We have no control over loss. It is how the packet switched network work.     
    - The network loss
        - It is that the IP datagram on the network does not arrive at where it is going to go. It is dropped at some point. It is due to the network congestion.        
    - The loss means
        - It is that the IP datagram on the network arrive at where it is going to go. The targeted end system received the datagram, but it didn't get there fast enough. It arrived there too late.        
    - The loss tolerate

12. The forward error correction FEC
    - It is the correction, but not the detection.        
    - We will to recover what was lost without retransmission.        
    - The simple FEC
        - we're going to create redundant chunks of every packet that we send and make copies of those redundant chunks and subsequent packets. We will take pieces of the packet and put them into subsequent packets.        
        - We can reconstruct what was lost. We can rebuild it on the receiver side.      
            - It is faster than that if we retransmit it.      
13. The interleaving to conceal loss
    - You take pieces of each packet to send. You have enough different pieces of packet. If you lose one of packet, you will manage to receive everything after it. You can rebuild different part. You can reconstruct it at the receipt.        

14. The Skype
    - It is the P2P.        
    - The super node
        - It keep track of the state of the network.        
    - Super peers 
        - It is that they keep a distributed index which includes a user's Skype name and IP address.        
    - The overlay network
        - It is a distributed hash table.        
    - The login server
    - The client
        - The first thing to join the network is to contact the super peer and it's assumed you would have this super peers address cache from previous interaction.        
        - You have connected to this super peers before the Super peers are going to be relatively steady.        
        - You can use the login server to receive the cache.        

15. Skype prefers to use UDP
    - Skype will use UDP and Skype will use forward error correction with UDP. 
    - UDP is not reliable.      
    - We have that hard upper bound on delay if we drop something with UDP, it's possible to rebuild it with forward error correction. So the Skype prefers to use UDP.        

16. Nats 
    - They are not going to route. They are not going to forward UDP.      


    







        






    

































