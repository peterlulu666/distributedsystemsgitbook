## Quiz      

1. The way to hide communication latencies
    - The asynchronous communication.
    - The prefetching. 

2. What is the asynchronous communication?
    - It defines a seperate handler for incoming response.
    - We take care of the task in the background and we present something else to the user.      

3. The prefetching.    
    - We will anticipate what the user is going to request. We will request and download the info before the user request it manually.     

4. The caching
    - Make copy of the data.

5. The consistency is the agreement.      
    - The inconsistency.
    - The global synchronization.
    - The amount of inconsistency we can tolerate is the application dependent.      

6. The pitfall
    - The false assumption.

7. The high performance computing

8. The grid computing

9. The cloud
    - The hardware.
    - The infrastructure.
    - The platform.
    - The application.        

10. The as a server
    - It is that the user can use the service without owning it.     




















