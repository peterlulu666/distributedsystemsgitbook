## Lecture      

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1iEmxIqWHR4MHkTf8-Ux0rF5l8f6gFCSV/preview"></iframe>

## Lecture Video



<iframe src="https://drive.google.com/file/d/1S0UEce6p5lI6x4KsJ1GU8Z5YatZzgAqW/preview" width="640" height="480"></iframe>

## Quiz        

1. The update
    - We propagate only a notification of an update
    - Transfer data from one copy to another
    - Propagate operation to other copy

2. The invalidation notices
    - We send a notice telling you that something in that replica is out of date
    - You can either **fetch that new data** immediately or you can actually wait. You wait until you know that you have to perform an operation on that particular data
        - If we **fetch that new data** immediately, meaning we get that invalidation notice from the primary replica
        - The **eager consistency**
            - It is that we are going to fetch that new data as soon as we get that invalidation notice
            - We are making it available to users **before** anyone actually **requests** it
            - It is the **preemptive**
                - If we **preemptively** download that data, we receive data before the requests. It is possible that we will retrieve that data and go through the overhead of actually getting the data to the in system
                - It is going to **increase overhead** on the network
            - With eager consistency, the user is going to be less aware of latency
            - The problem is the **lazy consistency** 
                - It is that we wait for an operation to actually be performed 
                - It is that we **wait** until a user **request** that data
                - We haven't done anything **preemptively** and the system has not automatically fetched anything. So the user is going to perceive latency
                - It **reduce overhead**
    - If there is **not a lot** of read write ratio, it is **efficient**

3. Transfer the data among replicas
    - If the read write ration is **large**, it is **efficient**
    - If there is a lot of write, it is **not efficient**
        - You push the data every single time it is updated. So if there is a lot of write, you have to push a lot of updates

4. The active replication propagate update operation
    - We send the list of update operations that they have to perform to the system
    - It allows us to specify a sequence of arbitrary length of update operations that they need to perform and we can push that sequence out to all of the replicas at the same time
        - The windows updates
            - If you apply an update to Windows, you have to go back to a previous update and you have to disable that update. There is two operations. We have to roll that update back and then we have to apply this new update

5. The **invalidation notice** is **one update**. It is the small scale operation

6. The **active replication** and **propagating the sequence of update operations** is the **longer sequence**. It is the sequence of updates

7. The pull push
    - The push based approach and the server based protocol
        - Propagate updates to other replicas without the replicas having to request the new data. It is the proactive
        - The server initiated
            - You immediately preemptively find all of your clients and push that update to the clients before the clients even know that what they have is out of date
    - The pull based approach and the client based protocol 
        - The client is driving the sequence
        - The client poll the server to receive data
        - It is caching
        - If it is not update very often, it is **efficient**
        - If there is a lot of update, it is **not efficient**
            - Every time you poll, there is overhead

8. The leases
    - It is the combination of push and pull based approaches
    - When the lease is **active**, a server is proactively going to **push** updates to that particular client. When the lease **expires**, the client is going to **pull** the server to see if there is any new data available. If yes, it is going to retrieve it
    - When the lease is active, it is the push based approach. When the lease expires, it is the pull based approach 

9. The categories of setting up primary based protocols
    - The remote write protocol
        - We pick one of the replicas. It is going to be the primary replica and we do not switch from that replica unless we have something like a failure
        - The coordinator is in the remote protocol. The coordinator is fixed
        - The client send the write operation to the primary replica. The primary replica is going to **coordinate and synchronize** the update across all of the replicas. The primary replica is going to push the write to the replica switches. It's going to block until it gets a response from all the replicas. Then it knows that that update has been applied everywhere
        - The pro is that this has a high degree of fault tolerance
            - Every replica is consistent to one another
        - It is the **blocking** operation 
            - The client is going to dispatch the update to its nearest replica. Then the client is going to wait until all of the replicas were updated before it could move on
        - we **forward** all of the requests back and forth to the primary. So we can go to the nearest replica first. The nearest replica would then relay all of that data
    - The local write protocol
        - We pick the primary replica. We **move** the primary replica around between replicas. We try to **move** the replica as **close** to that client as possible
        - The client start at its nearest replica. **Instead of forwarding** all of the requests off to the primary, we take the responsibility for **acting as the primary** and we are going to **move** it to the **nearest** replica
        - It is the **non blocking** approach
            - The client is going to dispatch the update. The update is going to be applied to the nearest replica at the same time that we are relocating the responsibilities for acting as the primary to the nearest replica. Then as soon as that update is applied to the nearest replica, we notify the client that it has been applied
            - the pros
                - The client does not have to wait. It can immediately get back to work after it has dispatched the update
            - The cons
                - It does not have the highest degree of redundancy

10. The fault tolerance
    - We have multiple nodes that are all working together. It is possible that some node is working and some node is not working. We want to handle the loss of a subset of nodes and still keep the other nodes doing useful work
    - It borrows a lot of concepts from the dependable system
        - The availability
        - The reliability
        - The safety
        - The maintainability

11. The availability
    - It is the system that is ready to be used immediately
    - It is that working correctly at any given moment

12. The reliability
    - It is that the system can run continuously without failure
    - It is that working correctly over a period of time

13. The availability is an instantaneous. It is the system that is running at this point

14. The reliability is an interval. It is how long does the system run between failures

15. The safety
    - If we experience the error, then we are not going to create a situation that we cannot recover from
    - We are not going to lose track of operations in such a matter that we cannot get that data back

16. The maintainability
    - It is how easily a failed system can be repaired
        - The self *

17. The error, failure and fault
    - The system is said to **fail** when it cannot meet its promises
    - The **error** is an aspect of a system state that could potentially lead to a failure
    - The cause of an error is a **fault**

18. The recurrence
    - The faults
        - There is **transient**, **intermittent** and **permanent**
        - The **transient** fault occurs once. Then it disappears. So the subsequent operation is correct
            - If the microwave on, it nukes the Wi-Fi connection for a second. If the microwave off, the Wi-Fi works correctly
        - The **intermittent** faults occur, go away and reappear
        - The **permanent** faults continue to exist until the faulty component is replaced
            - The physical component














































    

        
        

        
        
        
        



