## Lecture        

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1Bf6gGgsJyIZcHnXtryBF8SYZ64vPGCR8/preview"></iframe>

## Lecture Video



<iframe src="https://drive.google.com/file/d/1GSKu1wwenLu1_a2akNXDLO4sEz4sVeBN/preview" width="640" height="480"></iframe>

## Quiz        

1. Authentication Using Public Key Crypto
    - The public key
    - <img src="img/Authentication Using Public Key Crypto.png" alt="Authentication Using Public Key Crypto" width="500" height="600">

2. Digital Signatures
    - The hash function

3. The secure replicated servers
    - The byzantine fault agreement

4. The Kerberos
    - The shared key
    - It helps clients set up a secure channel with any server that is part of a distributed system
    - There are two primary components
        - The AS is the authentication server. It is responsible for handling the login request. It authenticates a user and provides a key that can be used to set up secure channel with other servers
        - The ticket granting service TGS is used to set up the secure channel. The TGS gives out tickets that servers could use to verify who the user actually is
    - <img src="img/Kerberos.png" alt="Kerberos" width="500" height="600">

5. The setting up a secure channel in Kerberos
    - Alice sends to Bob a message containing the ticket she got from the TGS, along with an encrypted timestamp. When Bob decrypts the ticket, he notices that Alice is talking to him, because only the TGS could have constructed the ticket
    - <img src="img/The setting up a secure channel in Kerberos.png" alt="The setting up a secure channel in Kerberos" width="500" height="600">

6. The access control
    - It is to verify the access right that you have been granted
    - <img src="img/The access control.png" alt="The access control" width="500" height="600">

7. The authorization is authenticating the identity and granting the right

8. The Access Control Matrix
    - The Access Control List ACL
        - The ACL keep track of which methods can be invoked on the object and who is permitted to invoke them
        - Each object maintain a list of the access rights of subjects that want to access the object
        - When subject sends request, it check against ACL. If subject is not in list, then they have no permissions
        - <img src="img/ACL.png" alt="ACL" width="500" height="600">
    - The capabilities
        - If you have the capabilities, it can be granted access. However, there is no identity that is associated with it 
        - It is just the client send request to server. The server do not check the identity. It only check if the capability is valid and if the requested operation is listed in the capability
        - If it is lost, you have to change the capabilities 

9. The firewall
    - The packet is routed through computer and it is inspected. It is going to filter the packet. It is going to discard the unauthorized traffic
    - The library firewall
    - It is implemented in the router

10. The Key Establishment
    - They do not have a public key
    - They just want to generate a key for use between themselves
    - Alice and Bob agree on two large number. It can be made available to the public. Alice picks her own secret large number. Bob picks his own secret large number
    - <img src="img/The Key Establishment.png" alt="The Key Establishment" width="500" height="600">

11. The certification authority
    - It is the inherently trusted organization. We can get the public key. The big company is the CA
    - If you want to publish public key, you can upload it to the CA and provide identity
    - There are no mandated procedures for identity verification
    - The CA combine public key with string that verify who you are, then sign it with private key
    - The signature of **private key** from the certificate authority **generates** what is called a **certificate**
    - You can get certificate from the certificate authority and then use the certificate authorities public Key to verify that the content is legitimate
    - The certificates are supposed to expire after a certain length of time
    - If it is going to be revoked sooner, there is certificate revocation list CRL
    - The client check the CRL













































































