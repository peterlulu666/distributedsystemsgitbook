## Lecture        

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1or8bl-8xiM9TbnYuPcr0APjoQQRbJeOZ/preview"></iframe>

## LEcture Video



<iframe src="https://drive.google.com/file/d/1Jh198ibocxDkPHHYzXcxfd2F6H6Z9hEx/preview" width="640" height="480"></iframe>

## Quiz        

1. The naming
    - It is finding something.        
        - We give entities the terminology to differentiate them from other entities. So we can identify which entity we are talking about. We can find the entity to work with when we need to.        
    - It is used to denote entity in the distributed system.      
    - To access the entity, we will access it at the access point. 
        - The access point is the entity named by address.      
        - The entity is printer. It is the file server.      
        - The location independent name is independent from the address of the access point offered by the entity.      
    - The pure name
        - It is the name that does not have meaning.        
        - It is the random string.        
        - It is used for comparison.        
        - The name can be changed.      
        - The address can be canged.      
        - The name can point to multiple entities.        
        - The entity can share the same name.     
    - The identifier
        - The identifier refer to at most one entity. It cannot be shared.            
        - The entity refer to at most one identifier.      
        - The identifier refer to the same entity.        
        - The identifier may not be the pure name.        
    - The flat name 
        - It is the name resolution.      
        - The broadcasting
            - It will not scale.     
            - It is always online. It require process listen to incoming location request.        
        - forwarding pointers
            - when an entity moves, it leave behind a pointer to the next location.      
            - It is that when an entity moves, it will go to the next location. Once it gets to its next location, you can go to wherever you were. You can leave behind a pointer to wherever you're presently at.      
            - whenever you move to another location, you put down the marker and say, I was here now if you would like to find me, please follow this marker and you can actually find where I am now.      
            - It can be made entirely transparent to client by the chain of pointers.        
                - The problem for the pointer to pointer is that there is not a huge amount of fault tolerance. It is like a linked list in memory, if you lose one pointer, the entire chain is no longer available to you.        
        - The home based approach
            - The home address register at naming service.      
            - The home register the foreign address.        
            - The client contact home location, and then foreign location.        

2. The name resolution
    - How to locate the entity that has that name right.       
    - How to find the name that we have the entity.        

3. The name space implementation
    - It is something like your hard drive is set up.      

4. The distributed hash table
    - The chord
        - It is organized into logical ring. We put them into ring due to it is how hash table work. When we go out the end of a hash table, we just circle back around to the beginning and start counting over.                 
        - The node is assigned random m bit identifier.        
            - If you have 3 bit identifier, you can support 8 different identifier.                
        - The entity is assigned unique m bit key.      
    - The finger table      
        - 0-1-2-3-4-5-6-7 circle
        - 1 2 4, 2 3 5, 3 4 6, 4 5 7, 5 6 0, 6 7 1, 7 0 2, 0 1 2
        - If the node is not available, what should we do?     
            - If there's no live system attached to that identifier and we can't point to the node. We will iterate around the circle until we find the address that is live.       
        - In the finger tables, no matter how large the network is, the last entry in the finger table will always point to the halfway across the network.       






        




















































