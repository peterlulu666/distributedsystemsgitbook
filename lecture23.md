## Lecture        

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/13mSG8CmbRwa25chQsRU3Q5-TZfa6Py8N/preview"></iframe>

## Lecture Video



<iframe src="https://drive.google.com/file/d/1PLzPorRramO41KXPu7OQUiIJEw6vda1e/preview" width="640" height="480"></iframe>

## Quiz        

1. The failure is that the computer is not meeting the promises

2. The crash failure
    - The crash failure occurs when a server prematurely halts, but it was working correctly until it stopped
    - It is apparent to everyone that system is down and is not responding

3. The omission failure
    - The omission failure occurs when a server fails to respond to a request
        - The receive omission failur
            - The server does not receive the request in the first place
        - The send omission failur
            - The send omission failure occurs when a server has done its work. But somehow failed to send the response
        - In the receive omission, the server's state never changed. It does not receive the request and it does not know what is going to do 
        - In the send omission, the server's state is changed. It receive the request. It get the things done. It just does not send the response

4. The timing failure
    - The timing failure occurs when a server's response lies outside the specified time interval
    - The timing failure is that the server responds correctly, but it is not fast enough
    - The delay loss
        - It is received by the recipient. However, there is hard upper bound

5. The response failure
    - The response failure is when the server's response is not correct
        - The value failure
            - The server receive the request. The server respond to it. The server send data to the client. However, it is not the data that the client is anticipating
            - The value failure is not the correct response
        - The state transition failure
            - The state transition failure is that the server receive the data. The server respond. Then the server deviated from the correct flow of control
        
6. The arbitrary failures
    - It is the byzantine failures
    - It is the convoluted problem that is not immediately apparent what is the problem. The server produces arbitrary responses at arbitrary times
    - You can identify the value failure immediately. However, you cannot identify the arbitrary failures immediately
    - with the arbitrary failure, if we get the wrong results some of the time, we may not be able to identify what we got is not what we expect

7. The flat group
    - It handle scalability as well
    - They are symmetrical and they are equal. If someone leaves the group, the group get smaller. If there is process join the group, the group get larger
    - The problem is that there are times when all of the processes need to agree on a certain course of action

8. The hierarchical group
    - The centralized algorithms that is substantially easier to handle when one process has taken on those additional roles as a coordinator
    - The coordinator is dispatching jobs to processes that are in the group. Each process have the responsibilities defined for it based on the algorithm. The job is dispatched to the coordinator. Then the coordinator identify what process in the network is best suited to handle the job and then it dispatch the process itself
    - The pros
        - There is coordinator. It is easier if we reach a consensus on something such as two phase commit
    - The cons
        - There is voting. We have to elect coordinator. If there is the single point of failure, then the coordinator goes down. Then we have to identify it is not there and get the new one

9. The faulty system
    - The synchronous versus asynchronous
        - If we are doing the same thing or not the same thing
    - The communication delay being bounded
        - The delay is **bounded** is that if we know that every message is going to be delivered with a globally predetermined maximum time
    - The message delivery being ordered
        - If we deliver the data, is it in the same order that we receive it
    - The message transmission done through unicasting or multicasting

10. The RPC semantics in the presence of failures
    - The client is unable to locate the server
    - The request message from the client to the server is lost
    - The server crashes after receiving a request
    - The reply message from the server to the client is lost
    - The client crashes after sending a request

11. The at least once
    - The operation was executed correctly at least one time. However, it may have been executed more than one time. So there is duplicate operations

12. The at most once
    - The client give up immediately and don't try again. It is not going to execute more than one time. So there is no duplicate operations. So it guarantee that you do not have duplicate operations

13. The clueless
    - It is easy to implement

14. The exactly once
    - It is that the operation is going to occur only once. So we are guaranteed that it happened and we are also guaranteed that there are no duplicates

15. We care about duplicates
    - The idempotent operations
        - We can safely perform multiple times
    





























    



