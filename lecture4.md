## Quiz      

1. What is the protocol used on router
    - The router is the layer 3 device, they do not care about the transport layer.

2. The application layer and the transport layer run on in system.      

3. The segment
    - It is the package of data in transport layer.   
    
4. The multiplexing
    - It is the taking multiple signal.      

5. The demultiplexing
    - The source IP address
    - The destination IP address
    - The transport layer segment
    - The source port number
    - The source port number
    - The direct segment to appropriate socket

6. The TCP and the UPD. We can write our own protocol. It is the end to end transport. The Internet does not care about it.      

7. The TCP
    - It is the in order delivery.
    - congestion control, it is about the Internet.
    - flow control, it is about the receiver. It ensure that it does not overwhelm the receiver. The TCP will ensure that the sender doesn’t send more data than the receiver can buffer.

8. The UDP best effort
    - It is unordered
    - The best effort is the one effort. The data maybe received out of order, the data maby lost, the UDP does not provide notification.     

9. The UDP connectionless
    - There is no handshaking.
    - The segment handled independently.
    - The connectionless is about UDP.      
    - The multi source goes to the same destination socket.

10. The TCP goes to different destination.    
    - It is easy to identify what data goes to the destination.

11. The TCP and the UDP do not provide delay guarantee and bandwidth guarantee.    

12. Why UDP
    - Fast, use the UDP for multimedia, it transmit faster, it send data faster.    
    - Simple, there is no connection state.       

13. The UDP is possible to be reliable
    - We should build it up into the application layer.      

14. The UDP checksum
    - The error detection.        

15. The rdt 3.0
    - It is up to the sender to identify if there is something wrong. So there is timeout.
    - Why there is sequence numbers?
        - If there is sequence numbers, then the receiver will find out whether an arriving packet contains new data or is a retransmission.
    - Why there is timers?
        - In order to identify if there is something wrong and identify if there is losses. If the ACK for a transmitted packet is not received within the duration of the timer for the packet, the packet (or its ACK or NACK) is assumed to have been lost. Hence, the packet is retransmitted.      

16. The setting the timeout
    - The short timeout setting, premature timeout.
    - The long timeout setting, it takes long time to identify the problem.

17. The pipelined protocol
    - The sender allow package that has been sent out, but has not been acknowledged.      


















