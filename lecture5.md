## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1w4rHng8Imc2h1ppGg1cVLuZHC4BVY180/preview"></iframe>

## Lecture Video

<iframe src="https://drive.google.com/file/d/1yp_tD6AD8RA8OAnE6jAa9kzgfBQ_F6C2/preview" 
width="640" 
height="480"
allowfullscreen="allowfullscreen"></iframe>

<iframe 
width="640" 
height="480"
src="https://drive.google.com/file/d/1VAxOLh7TqHhR-tMUY6ioVTDd7o8lL-nM/preview"></iframe>

## Quiz

1. The TCP point to point
    - The unicast.
    - one sender and one receiver.

2. The TCP reliable
    - The in order delivery, the receiver recieves the data in the same oder that the sender sent to.              
    - The byte stream, we can keep it open as long as we want and we can send as much data as required.

3. The TCP pipelined
    - The TCP is progressively trying to send more data into the network until it starts to loss packet and segment. When it starts to loss packet and segment, it will back off the amount of data it is sending. So it will not overwhelm the network.      
    - The TCP congestion control, the TCP is progressively trying to send more data into the network until it starts to loss packet and segment. When it starts to loss packet and segment, it will steeply drop way off. Then, the queue will take time to empty. So the sender will reduce the amount of data sending to the receiver very quickly. So it will not overwhelm the network.        

3. Why is TCP so aggressive with its congestion control? 
    - The TCP is going to increase the size of the congestion window by 1. And every time it receives something correctly, it will gradually increase the window. It will continue until there is loss and it will cut the window in half. So the window back off sharply. Thereafter, it will slowly build up and back off. So the TCP is aggressive with its congestion control. 
    - The queue fills up very quickly. When it fills up, it gets started dropping data very quickly. It takes a long time to empty all the way. So the TCP is aggressive with its congestion control.      
    - The TCP always wants to try to make use of all the throughput that is available. 
    - In order to ensure that it will not overwhelm the network. the TCP is aggressively trying to send more data into the network until it starts to loss data. When it starts to loss data, it will back off the amount of data it is sending. So it will not overwhelm the network.      
    - It is the important aspect of the TCP. This will allow us to have many TCP connection all running on the same network. 
    - There is congestion control built into it. You do not have one consuming the entire throughput and the capacity of the network.        

4. The TCP full duplex
    - The full duplex is that if we are going to have data going back and forth between the sender and the receiver, it is not one way communication, it is the bi **directional connection**. So the sender and the receiver can have a dialogue going back. They can actually be communicating data back and forth. They can do that on the same TCP connection.         
    - If we open one TCP connection, the sender can send data to the receiver and the receiver can use the same TCP to send the data back to the sender.        

5. The TCP connection oriented
    - The handshaking.      

6. The unicast, one to one. The multicast, one to many. The broadcast, one to all.   

6. What is the broadcast domain? 
    - The one to all transmission of data. Anything broadcast will be received by everyone one the network.      
    - The router break up the broadcast domain.

6. What is the multicast? Does TCP support the multicasting?
    - The multicast is one to many. There is one sender and multiple receiver. The TCP does not support the multicasting. The TCP is point to point and one to one. There is one sender and one receiver.      

7. The RTT, TCP round trip time
    - It is vary.     
    - If set the timer short, it will indicate there is problem whe nothing is wrong.
    - If set the timer long, it takes a long time to identify the problem.        

7. One of the ways reliable transport protocols detect error is the expiration of timers. 
    - The timer is short. The problem is the premature timeout. It will indicate there is problem whe nothing is wrong.     
    - The timer is long. It takes a long time to identify the problem.      

8. The TCP flow control
    - If you open the TCP, it creates the buffer at the transport layer.        
    - The buffer is the queue. The data has been sent to the process. It does not necessarily mean that the process is going to consume the data immediately.        
    - The receiver should notify the sender, so the sender will not overflow receiver's buffer.    
    - It is about the receiver. It ensure that it does not overwhelm the receiver. The TCP will ensure that the sender doesn’t send more data than the receiver can buffer.    

9. The data received and delivered        
    - The data can be received, but not delivered.        
    - The data received means that the data has been transmitted to the network and it has gone all the way across the network. It has physically arrived at the in system. It has been placed at the transport buffer.      
    - The data delivered means that the data has arrived at the receiver buffer and it has consumed the data.      

10. The TCP 3 way handshake       
    - The 2 way handshake problem, it will lead to bug and memory leak.      

11. When using the TCP is not appropriate?      
    - If it is the high reliability and high throughput link, we should not use the TCP. Let's say that in data center, if you are connecting component to one another. We should use the UDP. The UDP does not have the congestion control mechanism.      
    - If it is the medium that is inherently lossy, we should not use the TCP. The TCP have the congestion control mechanism. When the TCP loses a segment, it assumes that it loses the segment due to the congestion. If you are running data over the network, the TCP is great for that. However, let's say that the medium is the air. The air is not the reliable medium. So the medium itself is dropping data. You are losing segment due to the the air. You are not losing segment due to the congestion. However, the TCP does not know that. The TCP assumes that it loses the segment due to the congestion. So it is going to be stuck down.        

12. What is the result of mixing the UDP and the TCP?      
    - The TCP has the congestion control mechanism. The UDP does not have it. The UDP does not care about anything. 
    - As soon as you turn on the UDP, it will skyrocket to the absolute limite of the capacity and get started sending data as quickly as it possibly can. There is nothing to slow down the transmission. So it will stay there until you turn it off. 
    - The TCP will slowly build up and back off. It will repeat until you turn it off. The graph is the jigsaw shape.      
    













