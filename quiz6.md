## Quiz        

1. What is the purpose of the “initial permutation” in encryption algorithms?
    - Breaks up patterns in the input

2. If you do not trust a layer of the system stack (e.g., the TCP/IP protocol stack, plus the OS, middleware, and application layers), what must you do?
    - Implement security at a higher layer

3. Give an example of a security mechanism you could implement at the transport layer.
    - Build up the secure end to end tunnel. The data is encrypted at one system and only be decrypted at another system

4. Give an example of a security mechanism you could implement at the network layer.
    - We can write in blacklist IP address

5. Give an example of a security mechanism you could implement at the data link layer.
    - We can implement both white and black listing based on the MAC address

6. How is a subject’s identity encoded into a capability?
    - There is no identity that is associated with it

7. With regards to firewalls, why might you want to prevent traffic from entering your network?
    - You want to prevent the unauthorized traffic go into the network

8. There are three different focuses of control.  Name and briefly describe each.
    - To prevent the invalid operation. The API is there to control access and check the operation
    - To prevent the unauthorized operation. It check the API. It limit the API
    - To prevent the unauthorized user. It check who is the user. It provide API access to certain user

9. In general, there are two different types of firewalls.  What is the difference between the two?
    - Packet filter and application gateway

10. What happens if a subject does not appear in an object’s ACL?
    - They have no permission

11. How do entities prove their identities to Certification Authorities?
    - There is no mandated process

12. With regards to firewalls, why might you want to prevent traffic from leaving your network?
    - You want to protect the data and prevent the data leak. You do not want the data to go from the local system to the other system

13. Explain what is happening is happening on step 4 of the following diagram.
    - <img src="img/KDC Quiz.png" alt="KDC Quiz" width="500" height="600">
    - The key distributed center use the shared key to encrypt the challenge. The key is shared key. It is shared by A and KDC. The challenge contain the challenge RA1. It contian the identity B, so they know who it is. It contain the shared key KAB, so A is going to use it. It contain encrypted challenge KB,KDC(A,KAB,RB1), So A is going to send use it

14. What is the purpose of running the hash function of Alice’s computer?
    - <img src="img/Digital signature Quiz.png" alt="Digital signature Quiz" width="500" height="600">
    - It is going to produce the output and send it to Bob. It is her signature

15. Does the hash function ensure confidentiality?
    - No

16. What is the purpose of using Alice’s private key in this algorithm?
    - If you encrypt data with private key, it can only be decrypted by the public key. Bob is going to decrypt it using the public key. So if Alice use the private key, bob is going to know it is talking to Alice

17. Does using Alice’s private key ensure confidentiality?
    - No. Everyone has Alice's public key

18. How is Alice’s password transmitted across the network?
    - It is never transmitted

19. How is KA,AS stored on Alice’s workstation?
    - It is not stored
    - It is not stored in the work station. It is generated dynamically at the time input the password




































