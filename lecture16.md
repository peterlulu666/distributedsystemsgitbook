## Lecture        

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1BP1daOwb2KIIPe24e1YYr_hW7TXktaZW/preview"></iframe>

## Lecture Video



<iframe src="https://drive.google.com/file/d/1UvnS8POAiTL-A3mjpGBPsTWJ9NGCoVOk/preview" width="640" height="480"></iframe>

## Quiz        

1. The network proximity
    - It is that how close are these nodes to one another with regards to the physical topology of the network.        

2. How to measure distance on a network
    - We could measure distance based on the number of hops? We could measure how many routers do we have to go through in order to get to a certain location.      
    - We could measure distance in terms of latency.        

3. The topology aware assignment        
    - When assign ID to node, ensure that node in ID is close in network. It is difficult.        

4. The overlay networks may hide a lot of that distance.        

5. Hierarchical Location Service        
    - Build large scale tree for which sub network is divided into hierarchical domain. The domain is the directory node.      
    - Each domain have a directory node that keep track of entities in that domain.        
    - The tree
        - The leaf and intermediate node        
            - It store address.        
            - It keep a **location record** in the directory of the domain the **location record** keeps track of an entity's current address in that domain.      
            - The intermediate node contain the pointer to the child node. The leaf node store address of the entities.       
                - It support replicas.      
                - You start at the root to find the entity. You look up the entity at the next node. It know that the entity exists, but it does not know where it is and what is the address. So you go down to the child node and it will find where is the entity.       
            - The lookup
                - It start lookup at local leaf node.      
                - The node follow downward pointer, else go up.        
                - The upward lookup stop at root.      
            - The insert      
                - You creat the location record and iterate up the tree. Then look up for yourself. You made a copy either in this domain or it's somewhere in the sub tree.      
                - If you look up for yourself and you don't find anything, then you are going to create a record there. We do not keep the address. If you traverse down the child to look up for yourself until you find a record of yourself. Create replica from exists somewhere below us on the tree. Then you will edit the pointer. You can either find that entity by traversing down the right tree or the left tree.      
            - The delete operations work in reverse way.        
                - You start at present position in the network. If you want to delete the entry, you would traverse up the tree to look up for yourself and delete that the entry. You traverse up the tree and you remove element as you go. It is how you would remove element from the tree.        
an element from the tree        
        - The root node
            - The root node know about all the entities in the tree.        

6. The name spaces 
    - It is useful. it's a structured name 
        - The structured name is that if we look at the name, we can get information about where this item is located by looking at that name.      
    - It is a directed graph.   
        - It is a graph that the **leaf node** is the name entity and the **directory node** is the entity refer to the other node.               
            - The leaf node. 
                - The **leaf node** is a name entity and it doesn't have any edges coming out of it.      
            - The directory node
                - The **directory node** stores the table of pair. The **pair** is the edge label and the node identifier. The table is waht we call the **directory table**.        
    - The name space can have several root.      
    - The path name
        - We can describe a path in the particular namespace. You follow the edges labels list to get the entity, it is the path name.        
        - The absolute path name and relative path name
            - If the path name that start at root, it is the absolute path name. Otherwise, it is a relative path name.        
    - The node can store the attribute
        - The attribute describe the type of node.        
        - It include the type of entity, the identifier for the entity, address of entity, and nickname.        
        - The **directory node** can store the attribute. It store the **directory table** of the edge label and the node identifier **pair**.        

7. The closure mechanism
    - It is the mechanism to select the context from where to start the name resolution. It tell you where to start look up.          
    - What is our local DNS server
        - The DHCP is the closure mechanism to get the DNS server.        
        - When we log onto a network for the very first time, DHCP will give us our IP address. It will give us our default gateway, which is the address that we go to in order to get off of our subnet. It will tell us what is the local DNS server.        

8. The hard link the the actual path name. The soft link is the shortcut.        

9. The attribute based naming
    - The look up is expensive.        
    - Implement as database and combine with structured naming system.      




































   
