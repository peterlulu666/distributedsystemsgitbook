## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1p4qYE05cyuQWoxu-46JBo516sX1Yv6ix/preview"></iframe>

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1mk0FCSAFGmzFXT-LtIpbXK4HR89LH0Hj/preview"></iframe>

## Lecture Video

 

<iframe src="https://drive.google.com/file/d/18y6lvQ6-TEej7MafEdkCpkDwXTJ3pYkj/preview" width="640" height="480"></iframe>

## Quiz        

1. The layering
    - The user interface layer
    - The processing layer
    - The data layer

2. The single tiered, two tiered, and three tiered.

3. The structured P2P network
    - The connect node based on the criteria. 

4. The unstructured P2P network
    - The pick the node randomly.      

5. The superpeers role in the hybrid P2P network
    - It will maintain the index.
    - It will monitor the state of the network.    
    - It will track the system state.
    - It will track which node will have the data for you.      

6. The search unstructured P2P network, the flooding
    - It is the broadcast. It is the brute force solution. You send lookup query to the neighbor node. If all the neighbor node do not have the data you want, you will repeat the requesting until the lookup request is flooded.   
    - The problem of the flooding is that there is a lot of data going back and forth. So there is the redundant recipients. It does not accomplish anything with that amount of overhead.      
    - The limited flooding is that you will repeat the look up query with a fixed number of time.      
    - The probabilistic flooding is that you will repeat the look up query with a certain probability.      

7. The search unstructured P2P network, the random walk
    - It pick a neighbor at random. If the neighbor has the answer, it reply. Otherwise, it pick neighbor.       
    - It will work well with the replicated data.
    - Instead of looking for one data, there is the second data to look up.      
    - The benefit of the random walk is that it reduce the overhead.
    - The problem of the random walk is that it take a long time to find it.       

8. The content distribution network CDN
    - The challenge stream content to people simultaneously
        - The option 1, mega server, it does not scale.      
        - The option 2, multiple copy.
            - The enter deep.
            - The bring home.

9. The CDN cluster selection strategy
    - The challenge CDN DNS select CDN node to stream to the client.
    - The alternative, let the client select.      









