## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1fadWKaAvMqNwi-o2TyFrk57TMuLEfK4x/preview"></iframe>

## Lecture Video



<iframe src="https://drive.google.com/file/d/1EY0Njrj-4hYsk9qY0MGmXXXvWj2FwvMj/preview" width="640" height="480"></iframe>

## Quiz        

1. The transaction 
    - The sequence of operation.      

2. The ACID ensure that the operation succeed
    - The atomic operation
        - All operation succeed or fail. If the transaction fails, we will treat it as if nothing happens. We have to reset the system that it had before we tried to perform the transaction.   
        - We will treat the multiple operation as if there is one indivisible operation.
        - We either have to do every operation correctly or we have to reset all the operation.
        - The operation fail is called abort, it will reset the operation.            
    - The consistency operation
        - We have to make sure that all the operation work correctly.      
    - The isolation operation
        - If there is overlap of data we are going to to read and write. We will lock the data. We will make sure that only our transaction will modify it and make sure we do not interfere with other transaction.          
    - The durability operation
        - We write the data on the non volatile storage. If everything works, we will commit it.      

3. The TP Monitor
    - The decentralized system.        

4. The communication middleware
    - The remote procedure call.
    - The message oriented middleware.   

5. The pervasive system
    - They are divied into three category
        - The ubiquitous, everywhere you go, you can interact with the computer. It is waiting there in the background.        
        - The mobile, the system will not always be there, it change address and it change the access point.        
        - The sensor network, weather, traffic, noise, and pollution environment.    
            - The participatory sensing, waze.
    - The transparancy is not in the pervasive system
        - It is not true. 
        - There is the migration transparenc. The components are mobile and will need to re-establish connections when moving from one access point to another. Preferably, such handovers should be completely transparent to the user.  
    - There is more pervasive system
        - The home systems, electronic health-care systems, and sensor networks. 
        - Think of large- scale wireless mesh networks in cities or neighborhoods that provide services such as Internet access, but also form the basis for other services like a news system. 
        - There are systems for habitat monitoring, electronic jails by which offenders are continuously monitored, large-scale integrated sports systems, office systems deploying active badges to know about the whereabouts of their employees. 

6. The ubiquitous
    - It is networked, distributed, and accessible in a transparent manner.        
    - The interaction is unobtrusive.      
        - The voice interaction.      
        - The facial recognition.
    - The context awareness, the system will know what you are doing.          
        - The light sensor.
    - The autonomy.
        - There is no human intervention.      
    - The intelligence.
        - The system will cope with the dynamic action and interaction.        

7. The mobile
    - There is wireless communication.      
    - It will change the location.      
    - It is the disruption tolerant.      

8. The sensor network
    - The phone sensor.      

9. The architecture
    - The logically different system.      
        - The layer, modular
        - There is hierarchy for the layering.
        - There si no hierarchy for the object. There is direction for the object.        

10. The exchange data
    - The publish subscribe, decoupled in space.    
    - The shared dataspace, decoupled in time.      

11. The anonymous in term of information security
    - There is mechanism that will prevent you from obtaining the IP address and identifier info.      

12. The anonymous
    - We do not have use the IP address and the identifier.      

13. The decoupled in space is not the anonymous.
    - The anonymous is that we do not know who we are sending the data to.        
    - The decoupled in space is that we do not have explicit identifier when we send the data.      

14. The decoupled in time, asynchronous
    - The subscribed component does not have to be active and connected to the medium to receive the data.      










    





    












