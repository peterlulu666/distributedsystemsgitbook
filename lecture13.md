## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1UEEXSE5fL1kuu3x2n3aSf88eaJbfLQuv/preview"></iframe>

## Lecture Video

<iframe src="https://drive.google.com/file/d/1xHupl17pvwPGe4wfgBOv7bWRrNEKExon/preview" width="640" height="480"></iframe>

<iframe src="https://drive.google.com/file/d/1NjAHk0RjE_GSoZYMHUV1CVEXRVyqrYz3/preview" width="640" height="480"></iframe>
 
## Quiz      
 
1. The layer stack of the protocol
    - Application protocol
        - We build up the middleware below the application layer 
    - Presentation protocol
        - The access transparancy
        - The middleware
    - Session protocol
        - Track who is participating the communication session
        - It is the multicasting
        - The middleware
    - Transport protocol
        - We build up the middleware above the transport layer 
    - Network protocol
    - Data link protocol
    - Physical protocol

2. The middleware layer
    - The middleware provide the services for the autonomous systems participating in the distributed system.      
    - The communication protocol.
        - The marshaling
            - we would have demultiplexing in addition to identifying what all the components of this particular message are, how we are actually going to invoke that procedure or that method. 
        - The naming protocols 
            - How we find things.
            - The share resource.
        - The security protocols
            - The secure communication.
        - The scaling mechanisms
            - The replication and caching.

3. The types of communication
    - Transient vs persistent
        - Transient communication: Comm. server discards message when it cannot be delivered at the next server, or at the receiver.
        - The transient communications is that when you send data across a network, if the server is simply not ready for it, then the server will not get the data, so the data will disappear. The packet switch in the intermediate network will not store data. It will only forward the data.                 
        - Persistent communication: A message is stored at a communication server as long as it takes to deliver it. 
        - Persistent communication basically means that a message is stored at a communication server. And that message is going to be stored at that data Center for as long as it takes to actually deliver it to the targeted in system. 
    - Asynchrounous vs synchronous
        
4. We marshal the data for the request, we send the data across the network, it transmit the intermediate system and arrive at the server, it will complete the data request, and return the result to the client. 
    - Places for synchronization
        - The synchronize at request submission. 
            - The synchronization at request submission is a receipt and assurance that we have your message and we will attempt to deliver it. 
        - The synchronize at request delivery. 
        - The synchronize after request processing.

5. The intermediate network is packet switch and router. It will not store data. It will get the packet and forward it. 

6. The transient synchronous communication
    - Client and server have to be active at time of communication. 
    - Client issues request and blocks until it receives reply. 
    - Server essentially waits only for incoming requests, and subsequently processes them.
    - The disadvantage
        - The client can't really do anything while it's waiting for the server's response. 
        - Failures have to be handled immediately: the client is waiting. 
            - The failures transparancy. We want to hide failures from the user. And so it's our obligation to make sure that the user is not aware of the problem that is going on. 
        - The model may simply not be appropriate.
            - The human to human activity. 
7. The message oriented middleware
    - The persistent asynchronous communication
        - Processes send each other messages, which are queued. 
        - Sender need not wait for immediate reply, but can do other things. 
        - Middleware often ensures fault tolerance.
            - We don't expect an immediate response when we're using asynchronous communication, this is the benefit to fault tolerance. 
            - If there is something wrong, we don't have to identify that immediately. 
    - What happens here is that we want messages to be queued so that the sender does not have to wait for a response from the client. And the client doesn't have to be running at the same time to actually receive that message from the sender. 

8. The Remote Procedure Call RCP
    - We don't have specific send and receive functions in our code. 
    - We want to make things look like the function call. So there is send and receive. There is only the function call. 
    - it does not matter that if it is running remotely or locally. 

9. The parameter marshaling
    - There’s more than just wrapping parameters into a message
        - Client and server machines may have **different data representations**.
            - The byte ordered in the memory. The endianness. 
        - Wrapping a parameter means **transforming a value into a sequence of bytes**.
        - Client and server have to **agree on the same encoding**:
            - How are **basic data values** represented.
                - The integers, floats, characters.
            - How are **complex data values** represented.
                - The arrays, unions. 
        - Client and server need to **properly interpret messages**, transforming them into machine-dependent representations.

10. The asynchronous RPCs
    - Try to get rid of the strict request-reply behavior, but let the client continue without waiting for an answer from the server. 

11. The message queue is not the data server. 

12. The data stored in the data server is the long term data. 

13. The message broker
    - The source client prepare the message queue, the source client will place the message into incomming queue at the message broker, the message broker will implement the access transparancy with the conversion rules.
    - The message broker is the intermediate system. It is not the file server. 










 


























