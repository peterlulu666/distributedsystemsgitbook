## Quiz        

1. Briefly describe concurrent execution.        
    - The multiple process are swapping on and off a single core processor.     
    - The context switching.        

2. What is meant when something is offered “as a service”?      
    - The user can use the service even though they do not own the service.      

3. Give a brief example of how RPCs must implement access transparency.        
    - RPCs must convert between different data types, different units of measure, et cetera.      

4. What is meant by “self-*”?        
    - It is the automatic adaptivity.             
    - The star is the generic.        

5. There are generally three points of synchronization.  Name them.        
    - The synchronization at request submission.            
    - The synchronization at request delivery.              
    - The synchronization after request processing.        

6. What is the difference between a message server and a file server?        
    - The message server will place message in queue. It will get rid of the message if it get to the destination. It will not store it in the long term. It is the intermediate system. It will implement the access transparency.      
    - The file server is to store the data in the long term.      

7. What is meant by an API declaring a data structure “thread-safe”?  If a data structure is thread-safe, are all the data in the data structure also thread-safe?        
    - It means that the API itself will prevent concurrency issue from happening. We do not have to worry about locks, critical section, and mutexes.        

8. What is included in the processor context?      
    - There is stack pointer, program counter / instruction pointer, and page table register.              

9. What is included in the thread context?      
    - The processor context.      

10. What is meant by “structured P2P network”?      
    - Connect node based on the criteria.      

11. One of the tenets of ubiquitous computing is “unobtrusive interaction.”  Give a brief explanation of what this means.      
    - You can use the system and you do not have to pay attention to the system. It allow you to focus on something other than the system. 
    - It is like the voice interaction and the facial recognition.      

12. Briefly describe the concurrent computing.      
    - Multiple process that are running at the same time on the different core. The process do not interact.      

13. There are generally three levels of context.  Name them and give a brief description of them.      
    - The processor context, it is the value stored in the register of the processor. It contain the stack pointer, program counter, and the page table register.        
    - The process context, it is the memory mapping, it contain the thread context.          
    - The thread context, it contain processor context and it contain thread state.       

14. What is meant by “unstructured P2P network”?        
    - Pick the node at random.        

15. What is meant by being “decoupled in space”?  Is “decoupled in space” the same thing as being “anonymous”?      
    - The decoupled in space means that we do not have the explicit identifier when we send the data.      
    - The “decoupled in space” is not the “anonymous”.        

16. What is the difference between “transient” and “persistent” communications?      
    - The  “transient” communications is that when you send data across the network, if the server is not ready, then the server will not get the data, so the data will disappear. 
    - The“persistent” communications is that the message is going to be stored at the data center as long as it takes to deliver it to the targeted system.      
    - In the “transient” communications the intermediate system is there to forward data, it is not there to store data. In the “persistent” communications, the message will be stored in the data center.        

17. System functionality is frequently divided into three theoretical layers.  Name them and give a brief description of each.      
    - The user interface layer, it is the app user interface.      
    - The processing layer, it is the app function.      
    - The data layer, it is the app data.        

18. Give a brief definition of participatory sensing.      
    - When the user use the app, the user will be allowed to input info to the system at the same time, so the user will provide contrubution to the system, so the user is participating. So the system will understand what is going on and provide better service.      

19. Briefly define each element of the ACID criteria.      
    - The atomic operation, all operation succeed or fail. We treat multiple operaton as individual one. The operation will either be all correct or they will be reset.         
    - The consistency operation, ensure all operation work correctly.      
    - The isolation operation, if we read and write the overlap data, we will lock the data, so only our transaction will modify it, so we will not interfere with other transaction.      
    - The durability operation, We write the data on the non volatile storage. If everything works, we will commit it.           
        - The result of successful transactions are written to the non volatile storage.       




    
















