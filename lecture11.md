## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1gokCyOqCmxEtgIjsYDJvYO_ii9yfRMY0/preview"></iframe>

## Lecture Video



<iframe src="https://drive.google.com/file/d/1e7AfsOO_NFyTCHP8DDDmZLCql5BOeynl/preview" width="640" height="480"></iframe>

## Quiz        

1. The interceptor
    - It allow us to intercept when the local system make the call to the object and invoke the same function on the remote object.     
    - The using interceptors in adaptive middleware
        - We could use an interceptor to support mobility. In that case, a request-level interceptor would first look up the cur- rent location of a referenced object before the call is forwarded. 
        - Likewise, an interceptor can be used to transparently encrypt messages when security is at stake. Another example is when logging is needed. Instead of letting this be handled by the application, we could simply insert a method-specific intercep- tor that would record specific events before passing a call to the referenced object. More of such example will easily come to mind.  

2. The self-*
    - It is the automatic adaptivity. 
    - The star represent generic and can be anything.                    
    - We do not want the user to manually intervene to get the system to work.        
    - The self-configuration, it automatically decide that what will install.
    - The self-managing, if the system is disconnected, it will have application to recognize it automatically.   
        - In the unstructured P2p system, the nodes exchange membership information.    
    - The self-healing, if the system is disconnected, it will have application to recognize it automatically.      
    - The self-optimizing, the memory monitor, if the load is high, it will scale up to reduce the load on the server.      

3. The difference between the process and the thread
    - The process is seperated in memory.
    - The thread inherently share memory.      

4. The thread exist inside of the process.      

5. The process can fork process and can fork thread.      

6. The thread can fork thread, but it is not going to fork process.      

7. The processor context
    - It is the value stored in the registers of the processor.       

8. The thread context
    - It inherently contain the processor context.  
    - It contain the thread state.      
        - The sleep state
        - The active state
        - The wait state

9. The process context contain the thread context. The thread context contain the processor context.      

10. The context switching
    - The proccess switching is expensive.      
    - The creating and destroying thread is cheap.      

11. In the concept of context switch the problem
    - The OS will perform the context switch whenever it want.

12. The concurrency issue
    - The two thread run correctly, but the context switch allow us to have two successful increment operations that have resulted in the value only equaling one more than it did when we start.      
    - We will use lock to ensure it will not happen.      

13. The advantage and disadvantage of thread
    - The advantage, it is cheap and parallel.
    - The disadvantage, it is run concurrently.      

14. The thread safe
    - Thread safe API, API itself will prevent concurrency issuefrom happening. We do not have to worry about locks, critical sections, and mutexes.      

15. The thread safe data structure

16. The concurrent
    - The programming, program has multiple threads that are working on the same piece of data.
    - The execution, when multiple processes are swapping on and off a single core processor.    
        - The context switching.              
    - The computing, multiple processes that are running simultaneously on different cores. There are different processors, which do not interact.      



























