## Quiz 

1. The distribution transparency
    - The access transparency, hide the difference in data representation and the way of object accessed. We do not want our user to worry about how the data are represented. 
        - There are a lot image format. You go online to see the image. You do not care about what format the image was stored as. 
        - The directory structure on Unix and Windows are different. They have different location. They have different layout. We do not want our user to worry about that. 
    - Location transparency, hide where the object is located.
    - Relocation transparency, hide the fact that the object may be moved to another location when it is in use. 
    - Migration transparency, hide the fact that the object may move to another location.
    - Replication transparency.
    - Concurrency transparency.
    - Failure transparency.

2. What is the access transparency?
    - The access transparency, hide the difference in data representation and the way of object accessed. We do not want our user to worry about how the data are represented. 
    - There are a lot image format. You go online to see the image. You do not care about what format the image was stored as. 
    - The directory structure on Unix and Windows are different. They have different location. They have different layout. We do not want our user to worry about that. 

2. What is the portability of application?
    - It is the concept that the application can be moved to a different platform to do the same thing with minimum number of modification.
    - The JVM.

3. The policies and the mechanisms

4. The scalability





