## Quiz        

1. When a lease is active, how are updates propagated?  When a lease expires, how are updates propagated?
    - When it is active, the server proactively push update to the client. It is the push base
    - When it is expire, the client poll the server to see if there is any new data available. If yes, it retrieve. It is the pull base

2. What is meant by a “pull-based” approach to update propagation?
    - It is the client base. The client drive the sequence. The client poll the server to check the update
    - If the read to update ratio is low, it is efficient

3. In the context of invalidation notices, what is meant by “eager consistency”?
    - It is that we fetch new data as soon as we get the invalidation notices. We make it available to user before they request it. So user perceive less latency

4. What is meant by a “push-based” approach to update propagation?
    - It is that propagate update to other replica without the replica having to request the new data. So the server proactively push update to the client

5. Briefly describe the two consistency / update strategies a client can employ when it receives an invalidation notice.
    - You can fectch new data as soon as we get the invalidation notices. So it is the eager consistency. It is the preemptive.  We make it available to user before they request it. So user perceive less latency
    - You can wait until you perform the operation on the data. So it is the lazy consistency.  It is not the preemptive. It is not going to automatically fetch data. So user is going to perceive latency

6. Why is establishing group membership important in atomic multicasts?
    - It is that either the message be delivered to all process in the group or it is not delivered to any process in the group. So it is either eveyone or no one that deliver the message in the group. It is important to establish group and determine who is in the group
    - The replica can perform the operation only if they have reached agreement on the group membership

7. What client reissue strategy guarantees that an operation will be performed exactly one time?
    - There is no one

8. List, and give a brief definition, of the three categories of fault recurrence.
    - The transient fault recurrence, it occur once. Then it disappears. So the subsequent operation is right
    - The intermittent fault recurrence, it occur, go away, and reappear
    - The permanent fault recurrence, it continue to exist until the faulty component is replaced

9. Briefly describe the concept of “graceful degradation”.
    - If there is a lot of things going wrong, you can still get it to provide a few thing working with no problem

10. What category of failure may clients not be able to identify as a failure?
    - The arbitrary failure

11. What is one down side to the “reincarnation” strategy of addressing orphaned operations?
    - A network partition may prevent all systems from receiving the epoch notification

12. Briefly describe the “distributed commit” problem.
    - The problem is that either all participant successfully perform operation or it is rolled back at all participant. So we have to ensure that everyone can perform operation

13. Why are we concerned that an operation need be performed no more than one time?
    - In order to ensure no duplicate operation
    - In order to ensure the safety

14. How does a client determine why it did not receive a response from a server?
    - It is not able to

15. What is the purpose of three-phase commit (be specific)?  Are all versions of three-phase commit the same?
    - It improve the two phase commit in terms of recovery. It avoid blocking processes in the presence of crash
    - No

16. Define ‘value failure’ and ‘state-transition failure’.  What category of failure do these two specific failures belong to?
    - The value failure is that the server receive request. The server respond to it. The server send data to the client. However, it is not the data that the client is anticipating. The value of the response is wrong
    - The state transition failure is that the server receive the data. The server respond to it. Then the server deviated from the right flow of control
    - They belong to the response failure

17. The book presents four security mechanisms.  Briefly describe the purpose of authentication.
    - It is how we verify who is the entities
    - It is to prove you are who you say you are

18. The book presents four types of threats.  Briefly describe modification.
    - It is the unauthorized change to the data. So they change the data that is not supposed to be changed

19. The book presents four security mechanisms.  Briefly describe the purpose of auditing.
    - It is the analysis the vulnerabilities on the code

20. The book presents four types of threats.  Briefly describe fabrication.
    - Making illegitimate data appear legitimate

























    
    
